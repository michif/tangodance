//
//  soundObject.h
//  GeosoundsMarie
//
//  Created by Michael Flueckiger on 3.3.2014
//  Copyright (c) 2014 Michael Flückiger. All rights reserved.
//


#ifndef ColorWindows_ScreenSaverController_h
#define ColorWindows_ScreenSaverController_h

#include "ofMain.h"
#include "Poco/RegularExpression.h"
#include "ofxiOSSoundPlayer.h"

#include "ofxOpenALSoundPlayer.h"



/*
 struct soundPoint{
 ofVec2f myPosition;
 float distance;
 float direction;
 string myname;
 int myId;
 bool closest;
 bool isplaying;
 
 };*/


class SoundObjectBase{
	
public:
    
    SoundObjectBase ();
    ~SoundObjectBase ();
    

    
    
    static ofEvent<int> tick;


    
    void setup();
    void update();
    void draw();
    void clear();
    
    
    
    //We need to declare all this mouse events methods to be able to listen to mouse events.
    //All this must be declared even if we are just going to use only one of this methods.
    void mouseMoved(ofMouseEventArgs & args);
    void mouseDragged(ofMouseEventArgs & args);
    void mousePressed(ofMouseEventArgs & args);
    void mouseReleased(ofMouseEventArgs & args);
    
    
    void touchDown(ofTouchEventArgs & touch);
    void touchMoved(ofTouchEventArgs & touch);
    void touchUp(ofTouchEventArgs & touch);
    void touchDoubleTap(ofTouchEventArgs & touch);
    void touchCancelled(ofTouchEventArgs & touch);
    
    
    
    
    
    void loadSound(string fileName);
    void setSound(string fileName);
    void setTransposedSound(string fileName);
    
    void setBreak(string fileName);

    void setContentId(int _id);
    int getContentId();
    int myContentId;
    
    
    
    
    void setIndex(int _index);
    int getIndex();
    int myIndex;
    
    string myTitle;
    string mySoundfile;
    
    void setTitle(string _title);
    string getTitle();
    
    void setSoundfile(string _soundfile);
    void setSoundPath(string _soundfile);

    string getSoundfile();
    
    void setLoop(bool _loop);
    void setRemainInCircle(bool _remain);
    bool remain;
    
    
    void setColor(ofColor color);
    ofColor myColor;
    ofColor myPlayColor;

    ofColor myStopColor;
    
    
    ofxiOSSoundPlayer mySound;
    ofxiOSSoundPlayer myTransposedSound;

    ofxOpenALSoundPlayer sound;

    vector<ofxOpenALSoundPlayer*> alSounds;
    vector<ofxOpenALSoundPlayer*> breaks;

    
    ofxOpenALSoundPlayer * actualSound;

    

    vector<ofxiOSSoundPlayer*> sounds;

    
    
    vector<string>soundpath;
    
    int actualSoundId=0;


    
    int mySetSoundId=0;
    int lastPlayedSoundId=0;


    void setActualSoundId(int _id);
    int getActualSoundId();

    void setActualVariationOffset(int _offset);
    int getActualVariationOffset();
    
    void setVariationJump(int _jump);
    int variationJump;
    
    int getLastPlayedSoundId();
    
    
    
    float originalVolume;
    
    float fadeOut();
    float fadeIn();
    float fade();
    
    float fadespeed;
    float fadeTo(float fadeTarget,float _actualFade);
    
    float soundfileFade();

    
    
    void startFade(float _target);
    void startFade(float _target, float _actualFade);

    
    void endFade();
    
    void setFadeTarget(float _target);
    float fadeTarget;
    
    float teaserFadeTarget;
    float soundFadeTarget;
    
    float actualFade;
    float fadeamount;
    
    void setMute(bool _mute);
    bool getMute();
    
    // ofVec3f center;
    
    void setFadeSpeed(float _speed);
    
    void setMinDistMute(float _mindist);
    float getMinDistMute();
   
    void drawArcStrip(float percent, ofVec2f center, float radius);

    void addToQueue();
    void removeFromQueue();
    
    void setIsBreak(bool _isbreak);
    bool getIsBreak();
    void unsetIsBreak();
    bool bIsBreak;
    
    void instantPlay();

    
    void onTick(int & e);
    void onStart(int & e);
    void onHalf(int & e);
    void onStop(int & e);


    
    void switchSound(int _id);
    
    
    void resetPlay();
    void pausePlay();

    
    ofVec2f screenpos;
   void setScreenpos(ofVec2f _pos);
    int height;
    
    float getMySoundLength();
    bool getIsLoaded();
    
    void loadSoundFileById(int _id);
    
    void setIsBaseBeat(bool _bIsBase);

    
    void setBlock(int _ticks);
    
    
    
    //MovingObject fadeObject;
    
private:
    float bIsBase;

    
    float muteVolume;
    bool bismute;
    bool isfadeing;
    bool bRegisteredEvents;
    float heading;
    float soundvolume;
    float soundpos;

    
    bool snapToGrid;
    bool isAddedToQueue;
    
    float mySoundLength;
    
    
    bool bIsLoaded;
    bool bTransposedIsLoaded;
    
    
    
    float actualVariationOffset; //-> Zum hin und her Switchen der Päckchen...
    
};

#endif
