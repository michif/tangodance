#pragma once

#include "ofMain.h"
#include "ofxiOS.h"
#include "ofxiOSExtras.h"
#include "ofxCoreMotion.h"
#include "soundcontroller.h"


#define DELAY 800
#define ENERGYDRAG 0.98


class ofApp : public ofxiOSApp {
	
    public:
        void setup();
        void update();
        void draw();
        void exit();
	
        void touchDown(ofTouchEventArgs & touch);
        void touchMoved(ofTouchEventArgs & touch);
        void touchUp(ofTouchEventArgs & touch);
        void touchDoubleTap(ofTouchEventArgs & touch);
        void touchCancelled(ofTouchEventArgs & touch);

        void lostFocus();
        void gotFocus();
        void gotMemoryWarning();
        void deviceOrientationChanged(int newOrientation);
    
    
    
    
    ofVec3f movespeed;
    float moveenergy;
    
    float px;
    float py;
    float pz;
    
    
    float startTime; // store when we start time timer
    float endTime; // when do want to stop the timer
    
    bool  bTimerReached; // used as a trigger when we hit the timer
    
    
    float roll;
    
    int numSteps;
    
    bool doubletab;
    bool isSleeping;
    
        
    
      ofxCoreMotion coreMotion;
    
    
    
    
    

    
    

};


