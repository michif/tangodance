#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetCircleResolution(40);
    
    SC->setup();
    
    
 //   ofxiPhoneSetOrientation(OFXIOS_ORIENTATION_LANDSCAPE_RIGHT);
    
    coreMotion.setupMagnetometer();
    coreMotion.setupGyroscope();
    coreMotion.setupAccelerometer();
   // coreMotion.setupAttitude(CMAttitudeReferenceFrameXMagneticNorthZVertical);
    coreMotion.setupAttitude(CMAttitudeReferenceFrameXArbitraryZVertical);
    doubletab=false;
    isSleeping=false;

    px=py=pz=0;
    numSteps=0;
    
    bTimerReached = false;
    startTime = ofGetElapsedTimeMillis();  // get the start time
    endTime = (int)ofRandom(1000, 5000); // in milliseconds
    
    ofSetFrameRate(60);
    moveenergy=0;
    //ofSetVerticalSync(true);
    
}

//--------------------------------------------------------------
void ofApp::update(){
    SC->update();

    coreMotion.update();
   //   roll=ofMap(coreMotion.getRoll(),-3,3,36,52,true);
    roll=ofMap(ABS(coreMotion.getRoll()),0,3,39,50,true);

   // cout<<roll<<endl;

    movespeed+=ABS(coreMotion.getUserAcceleration().length());
    movespeed*=0.98;
    
    
    moveenergy+=ABS(coreMotion.getUserAcceleration().length());
    moveenergy*=ENERGYDRAG;
    
    ofVec3f acc = coreMotion.getAccelerometerData();

    float xx=acc.x;
    float yy=acc.y;
    float zz=acc.z;
    
    float dot=(px*xx)+(py*yy)+(pz*zz);
    float a=ABS(sqrt(px*px+py*py+pz*pz));
    float b=ABS(sqrt(xx*xx+yy*yy+zz*zz));
    
    dot/=(a*b);
    
   // cout<<"dot"<<dot<<endl;
    if(dot<=0.9){
    }
    px=xx;
    py=yy;
    pz=zz;
    
    float timer = ofGetElapsedTimeMillis() - startTime;
    if(timer>DELAY){
        isSleeping=false;
    }
    
    
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    /*
    
    // attitude- quaternion
    ofDrawBitmapStringHighlight("Attitude: (quaternion x,y,z,w)", 20, 25);
    ofSetColor(0);
    ofQuaternion quat = coreMotion.getQuaternion();
    ofDrawBitmapString(ofToString(quat.x(),3), 20, 50);
    ofDrawBitmapString(ofToString(quat.y(),3), 90, 50);
    ofDrawBitmapString(ofToString(quat.z(),3), 160, 50);
    ofDrawBitmapString(ofToString(quat.w(),3), 230, 50);
    
    // attitude- roll,pitch,yaw
    ofDrawBitmapStringHighlight("Attitude: (roll,pitch,yaw)", 20, 75);
    ofSetColor(0);
    ofDrawBitmapString(ofToString(coreMotion.getRoll(),3), 20, 100);
    ofDrawBitmapString(ofToString(coreMotion.getPitch(),3), 120, 100);
    ofDrawBitmapString(ofToString(coreMotion.getYaw(),3), 220, 100);
    
    // accelerometer
    ofVec3f a = coreMotion.getAccelerometerData();
    ofDrawBitmapStringHighlight("Accelerometer: (x,y,z)", 20, 125);
    ofSetColor(0);
    ofDrawBitmapString(ofToString(a.x,3), 20, 150);
    ofDrawBitmapString(ofToString(a.y,3), 120, 150);
    ofDrawBitmapString(ofToString(a.z,3), 220, 150);
    
    // gyroscope
    ofVec3f g = coreMotion.getGyroscopeData();
    ofDrawBitmapStringHighlight("Gyroscope: (x,y,z)", 20, 175);
    ofSetColor(0);
    ofDrawBitmapString(ofToString(g.x,3), 20, 200 );
    ofDrawBitmapString(ofToString(g.y,3), 120, 200 );
    ofDrawBitmapString(ofToString(g.z,3), 220, 200 );
    
    // magnetometer
    ofVec3f m = coreMotion.getMagnetometerData();
    ofDrawBitmapStringHighlight("Magnetometer: (x,y,z)", 20, 225);
    ofSetColor(0);
    ofDrawBitmapString(ofToString(m.x,3), 20, 250);
    ofDrawBitmapString(ofToString(m.y,3), 120, 250);
    ofDrawBitmapString(ofToString(m.z,3), 220, 250);
    
    
    ofSetColor(255,0,0);
    ofPushMatrix();
    ofRect(0,100, moveenergy*10, 10);
    ofPopMatrix();
    
    ofPushMatrix();
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
    
    // 1) quaternion rotations
    float angle;
    ofVec3f axis;//(0,0,1.0f);
    quat.getRotate(angle, axis);
   ofRotate(angle, axis.x, -axis.y, axis.z); // rotate with quaternion
    
    // 2) rotate by multiplying matrix directly
    //ofMatrix4x4 mat = coreMotion.getRotationMatrix();
    //mat.rotate(180, 0, -1.0f, 0);
    //ofMultMatrix(mat); // OF 0.74: glMultMatrixf(mat.getPtr());
    
    // 3) rotate with eulers
    ofRotateX( ofRadToDeg( coreMotion.getPitch() ) );
    //ofRotateY( -ofRadToDeg( coreMotion.getRoll() ) );
   // ofRotateZ( ofRadToDeg( coreMotion.getYaw() ) );
    
    ofNoFill();
	ofDrawBox(0, 0, 0, 220); // OF 0.74: ofBox(0, 0, 0, 220);
    ofDrawAxis(100);
    ofPopMatrix();
    
    ofFill();
    ofDrawBitmapString(ofToString("Double tap to reset \nAttitude reference frame"), 20, ofGetHeight() - 50);
     */
    
    SC->draw();

    
	
}





//--------------------------------------------------------------
void ofApp::exit(){

}

//--------------------------------------------------------------
void ofApp::touchDown(ofTouchEventArgs & touch){
    


}

//--------------------------------------------------------------
void ofApp::touchMoved(ofTouchEventArgs & touch){

}

//--------------------------------------------------------------
void ofApp::touchUp(ofTouchEventArgs & touch){

 
}

//--------------------------------------------------------------
void ofApp::touchDoubleTap(ofTouchEventArgs & touch){

  //  coreMotion.resetAttitude();
    
}

//--------------------------------------------------------------
void ofApp::touchCancelled(ofTouchEventArgs & touch){
    
}

//--------------------------------------------------------------
void ofApp::lostFocus(){

}

//--------------------------------------------------------------
void ofApp::gotFocus(){

}

//--------------------------------------------------------------
void ofApp::gotMemoryWarning(){

}

//--------------------------------------------------------------
void ofApp::deviceOrientationChanged(int newOrientation){
  if(newOrientation==OFXIOS_ORIENTATION_PORTRAIT){
      
  //ofBackground(200);
      SC->isUpright=true;
//SC->coreMotion.resetAttitude();
}
  else{
      SC->isUpright=true;
     // SC->coreMotion.resetAttitude();

 // ofBackground(0, 0, 255);
      
    
      
  }

}
