//
//  soundcontroller.cpp
//  Geosounds
//
//  Created by Michael Flueckiger on 16.04.12.
//  Copyright (c) 2014 Michael Flückiger. All rights reserved.
//

#include "SoundObjectBase.h"
#include "soundcontroller.h"



float origfadeamount=0.01;
float scalefact=800;

SoundObjectBase :: SoundObjectBase ()
{
    bIsBase=false;
    cout << "creating SoundObjectBase" << endl;
    originalVolume=1;
   
    /*fadeamount=0.1;
    actualFade=0;
    isfadeing=false;
    fadespeed=0.05;
    bismute=false;
    muteVolume=0.1;
    fadeTarget=1;*/

    bRegisteredEvents = false;
    
    snapToGrid=true;
    isAddedToQueue=false;
    screenpos.set(0,0);
    
    height=50;
    remain=false;

    actualSoundId=0;
    
    bIsLoaded=false;
    bIsBreak=false;


}

SoundObjectBase :: ~SoundObjectBase ()
{
    cout << "destroying SoundObjectBase" << endl;
    clear();

}



//--------------------------------------------------------------
void SoundObjectBase::setup(){
   
    soundvolume=0;
    actualSoundId=0;
    
    if(!bRegisteredEvents) {
        ofRegisterMouseEvents(this); // this will enable our circle class to listen to the mouse events.
        ofRegisterTouchEvents(this); // this will enable our circle class to listen to the mouse events.
        bRegisteredEvents = true;
    }
    

    
 //  ofAddListener(SC->tick , this, &SoundObjectBase::onTick);
    //listening to this event will enable us to get events from any instance of the circle class as this event is static (shared by all instances of the same class).

    ofAddListener(SC->cycleEnded,this,&SoundObjectBase::onTick);
    ofAddListener(SC->cycleStart,this,&SoundObjectBase::onStart);
    ofAddListener(SC->cicleHalf,this,&SoundObjectBase::onHalf);
    ofAddListener(SC->cycleStop,this,&SoundObjectBase::onStop);

    //bIsLoaded=false;
 //   fadeObject.setup();

   /* fadeObject.setTarget(ofVec3f(100,0,0));
    fadeObject.setMaxSpeed(5);
    fadeObject.setAccelerated(false);
    fadeObject.setPosition(ofVec3f(0,0,0));
*/
  }



//--------------------------------------------------------------
void SoundObjectBase::onTick(int & e){
      cout<<"it Ticked "<<e<<endl;
    if(isAddedToQueue){
       // resetPlay();
       // removeFromQueue();
    }
}


//--------------------------------------------------------------
void SoundObjectBase::onStart(int & e){
    cout<<"it started"<<e<<endl;
    
    
    
    if(isAddedToQueue){
    
        resetPlay();
        removeFromQueue();
    }
    
}


//--------------------------------------------------------------
void SoundObjectBase::onHalf(int & e){
    cout<<"its half"<<e<<endl;
}

//--------------------------------------------------------------
void SoundObjectBase::onStop(int & e){
    cout<<"it stopped"<<e<<endl;
        pausePlay();
        removeFromQueue();
    
}

//--------------------------------------------------------------
void SoundObjectBase::update(){
  //  fadeObject.update();
 //   if(sounds[actualSoundId]->isLoaded() && !bIsLoaded ){
 /*   mySound.play();
    mySound.setPosition(0.9999);
    float ms = mySound.getPositionMS();
    mySoundLength=ms;
    mySound.setPosition(0);
    mySound.stop();
    bIsLoaded=true;*/
   // }
    
    
      /* //loadSound
    if( isInEarShot(arcDist, radius)){
        if(!mySound.isLoaded()){
            setSound(mySoundfile);
        }
        if(mySound.isLoaded()){
           if(!mySound.getIsPlaying()) mySound.play();
            if(!myTeaser.getIsPlaying()) myTeaser.play();
        }
    }*/
    
  
    
    //unloadSound
   /* if( !isInEarShot(arcDist, radius)){
        mySound.stop();
        myTeaser.stop();
        if(mySound.isLoaded())mySound.unloadSound();
        if(myTeaser.isLoaded()) myTeaser .unloadSound();
        
    }*/
    
   // cout<<alSounds[0]<<endl;
    //if(alSounds[0]->isLoaded()&&mySoundLength==0){
        
    //    mySoundLength = alSounds[0]->length/44100/4;

        
      //  alSounds[0]->setPosition(0.5f);
       // mySoundLength = 2 * alSounds[0]->getPositionMS(); // wink
       // alSounds[0]->setPosition(0.0f);
       // cout<<"--------------"<<mySoundLength<<endl;
       // SC->baseCycleDuration=mySoundLength;
   // alSounds[0]->play();
    //}

    
  //  alSounds[1].play();
   // if(sounds[actualSoundId]->isLoaded()){
        
       /* //  float dotP = leftRight.dot(delta);
        float dotPArc = leftRight.dot(arcDelta);
        
        if(arcDist>0.005){
            mySound.setPan(dotPArc);
        }
        else{
            mySound.setPan(0.5);
        }
        mySound.setPan(dotPArc);
        */
        

        //if(sounds[actualSoundId]->getIsPlaying())actualFade=fadeTo(fadeTarget,actualFade);
        //cout<<actualFade<<endl;
        // Set Volumes
      //  sounds[actualSoundId]->setVolume(actualFade);
        
       
      
        
  //}
}


//--------------------------------------------------------------
void SoundObjectBase::draw(){
  //  if(sounds[actualSoundId]->isLoaded()){
   // soundpos=sounds[actualSoundId]->getPosition();
   // }
   // cout<<actualSoundId<<endl;
    if(alSounds.size()>0){
       // cout<<"sound id"<<actualSoundId<<" is loaded "<<alSounds[actualSoundId]->isLoaded()<<endl;
    if(alSounds[actualSoundId]->isLoaded()){
        soundpos=alSounds[actualSoundId]->getPosition();
    }
  //  fadeObject.draw();
   
    ofSetColor(myStopColor);
    if(isAddedToQueue){
        ofSetColor(myPlayColor);
    }

    ofPushMatrix();
    ofTranslate(screenpos.x, screenpos.y, 0);
    ofRect(0, 0, ofGetWidth(), height);
    ofTranslate(ofGetWidth()/2,height/2, 0);
    ofSetColor(255);
    drawArcStrip(soundpos,ofVec2f(0,0),25);
        ofDrawBitmapStringHighlight(ofToString(actualSoundId,3), -50,0);

    ofPopMatrix();
    
//    fadeObject.draw();
    

}


}


void SoundObjectBase::setFadeSpeed(float _speed){
    fadespeed=_speed;
    
}




//--------------------------------------------------------------
void SoundObjectBase::loadSound(string fileName){
    mySound.loadSound(fileName);
}


void SoundObjectBase::loadSoundFileById( int _id){
    string mypath="sounds/";
    string fileName=soundpath[_id];
    mypath+=fileName;
    mySound.loadSound(ofToDataPath(mypath,true));
}


void SoundObjectBase::setSoundfile(string _soundfile){
    mySoundfile=_soundfile;
   // soundpath.push_back(_soundfile);
}


string SoundObjectBase::getSoundfile(){
    return mySoundfile;
}


void SoundObjectBase::setSoundPath(string _soundfile){
     soundpath.push_back(_soundfile);
}


void SoundObjectBase::setActualVariationOffset(int _offset){
    actualVariationOffset=_offset;
  actualSoundId= mySetSoundId+actualVariationOffset;
}

int SoundObjectBase::getActualVariationOffset(){
    return actualVariationOffset;
}


void SoundObjectBase::setVariationJump(int _jump){
    variationJump=_jump;
}

void SoundObjectBase::setActualSoundId(int _id){
    actualSoundId = _id+actualVariationOffset;
    mySetSoundId=_id;
    //mySound.loadSound(ofToDataPath(mypath,true));
}

int SoundObjectBase::getActualSoundId(){
    return actualSoundId;
}





void SoundObjectBase::switchSound(int _id){
    actualSoundId = _id+actualVariationOffset;
    mySetSoundId=_id;
    float pos=actualSound->getPosition();
    bool isPlaying=actualSound->getIsPlaying();
    if(isPlaying){
    actualSound->stop();
    }
    actualSound=alSounds[actualSoundId];
    if(isPlaying){
    actualSound->play();
    }
    actualSound->setPosition(pos);
}

//--------------------------------------------------------------
void SoundObjectBase::setSound(string fileName){
       cout<<"loading sound "<<"sounds/"+fileName<<endl;


    try
    {
        string mypath="sounds/";
        mypath+=fileName;
        
        
        cout<<"loading sound "<< ofToDataPath(mypath,true)<<endl;
        
        ofxOpenALSoundPlayer * s =new ofxOpenALSoundPlayer();
        s->loadSound(ofToDataPath(mypath,true),true);
        s->setMultiPlay(true);
        mySoundLength = s->length/44100/4*1000;
        cout<<"length "<<mySoundLength<<endl;

        bIsLoaded=true;
        alSounds.push_back(s);
        actualSound=alSounds[0];
    }
    catch (int e)
    {
        cout << "An exception occurred. Exception Nr. " << e << '\n';
    }
    
}


//--------------------------------------------------------------
void SoundObjectBase::setTransposedSound(string fileName){
    cout<<"loading sound "<<"sounds/"+fileName<<endl;
    
    
    try
    {
        string mypath="sounds/";
        mypath+=fileName;
        cout<<"loading sound "<< ofToDataPath(mypath,true)<<endl;
        myTransposedSound.loadSound(ofToDataPath(mypath,true));
        //myTransposedSound.play();
        //myTransposedSound.setPosition(0.9999);
       // float ms = myTransposedSound.getPositionMS();
        //mySoundLength=ms;
        //myTransposedSound.setPosition(0);
       // myTransposedSound.stop();
        
        
        ofxOpenALSoundPlayer * s =new ofxOpenALSoundPlayer();
        s->loadSound(ofToDataPath(mypath,true),true);
        s->setMultiPlay(true);
        cout<<"length "<<mySoundLength<<endl;
        //SC->baseCycleDuration=mySoundLength;
        bTransposedIsLoaded=true;
        alSounds.push_back(s);

        
        
       // sounds.insert(sounds.begin() + 1, myTransposedSound);
        //sounds.push_back(&myTransposedSound);
    }
    catch (int e)
    {
        cout << "An exception occurred. Exception Nr. " << e << '\n';
    }
    
    
    
  //  myTransposedSound.setLoop(false);
   // myTransposedSound.setVolume(0);
   // soundvolume=0;
    
}


//--------------------------------------------------------------
void SoundObjectBase::setBreak(string fileName){
    cout<<"loading break "<<"sounds/"+fileName<<endl;
    
    
    try
    {
        string mypath="sounds/";
        mypath+=fileName;
        
        
        cout<<"loading sound "<< ofToDataPath(mypath,true)<<endl;
        
        ofxOpenALSoundPlayer * s =new ofxOpenALSoundPlayer();
        s->loadSound(ofToDataPath(mypath,true),true);
        s->setMultiPlay(true);
        mySoundLength = s->length/44100/4*1000;
        cout<<"break length "<<mySoundLength<<endl;
        breaks.push_back(s);
    }
    catch (int e)
    {
        cout << "An exception occurred. Exception Nr. " << e << '\n';
    }
    
}



void SoundObjectBase::setIsBreak(bool _isbreak){
    bIsBreak=_isbreak;
}


bool SoundObjectBase::getIsBreak(){
    return bIsBreak;
}

float SoundObjectBase::getMySoundLength(){
    return mySoundLength;
}


//--------------------------------------------------------------
void SoundObjectBase::setRemainInCircle(bool _remain){
    remain=_remain;
}


//--------------------------------------------------------------
void SoundObjectBase::setLoop(bool loop){
    mySound.setLoop(loop);
}

//--------------------------------------------------------------
void SoundObjectBase::addToQueue(){
    if(bIsBase){
        //mySoundLength = alSounds[actualSoundId]->length/44100/4*1000;
        // cout<<"set New Length  "<< mySoundLength<<endl;
      //  SC->baseCycleDuration=mySoundLength;
    }
    isAddedToQueue=true;
   // loadSoundFileById(actualSoundId);
    }

//--------------------------------------------------------------

void SoundObjectBase::removeFromQueue(){
    isAddedToQueue=false;
}

//--------------------------------------------------------------
/*Play Sound without wating for soundcircle */
void SoundObjectBase::instantPlay(){
    if(!actualSound->getIsPlaying()){
    isAddedToQueue=true;
    resetPlay();
    removeFromQueue();
    }
}


//--------------------------------------------------------------
void SoundObjectBase::setContentId(int _id){
    myContentId=_id;
    cout <<myContentId<<endl;
}

//--------------------------------------------------------------


int SoundObjectBase::getContentId(){
    return myContentId;
};



void SoundObjectBase::setIndex(int _index){
    myIndex=_index;

}

int SoundObjectBase::getIndex(){
    return myIndex;
}


void SoundObjectBase::setTitle(string _title){
    myTitle=_title;
}


string SoundObjectBase::getTitle(){
    return myTitle;
}


bool SoundObjectBase::getIsLoaded(){
    
    return bIsLoaded;
}


void SoundObjectBase::startFade(float _target){
    isfadeing=true;
    fadeTarget=_target;
    fadeamount=origfadeamount;
}


void SoundObjectBase::startFade(float _target, float _actualfade){
    isfadeing=true;
    fadeTarget=_target;
    fadeamount=origfadeamount;
    actualFade=_actualfade;
}

void SoundObjectBase::endFade(){
    isfadeing=false;
}


float SoundObjectBase::fade(){
    if(isfadeing){
    actualFade+=fadeamount;
        if(actualFade>1 || actualFade<0)endFade();
    actualFade=ofClamp(actualFade,0,1);
    }
     return actualFade;
}

void SoundObjectBase::setFadeTarget(float _target){
    fadeTarget=_target;
}


float SoundObjectBase::fadeTo(float _fadeTarget, float _actualfade){
    float target =_fadeTarget;
    float actualfade=_actualfade;
    float fadeDistance = target-actualfade;
    if(isfadeing){
    if(abs(fadeDistance)<0.01){
        actualfade=target;
        endFade();
        return actualfade;
    }
    float fadefact=fadeDistance*fadespeed;
    actualfade+=fadefact;
    }
    return actualfade;
}



void SoundObjectBase::resetPlay(){
    
    if(bIsBase){
    int offset=SC->getVariationOffset();
    int myActualJump= offset*variationJump;
    
    int soundid=SC->getChordOffset();
    cout<<SC->getIsBreak()<<endl;
    
    setIsBreak(SC->getIsBreak());
   // bool se=SC->getIsBreak();
    
    for (int i=0;i<alSounds.size();i++){
        alSounds[i]->stop();
    }
    
    actualSound->stop();
    
    
    
    if(getIsBreak()){
        cout<<"is break"<<endl;
        if(offset>breaks.size()-1)offset=breaks.size()-1;
        if(!breaks.size()==0){
        actualSound= breaks[offset];
        }
        setIsBreak(false);
        SC->setIsBreak(false);
    }else{
        int mysound=soundid+myActualJump;
        if(mysound>alSounds.size())mysound=alSounds.size()-1;
        actualSound=  alSounds[mysound];
    }
    
        
    }else{
        actualSound=alSounds[0];
    }
    
    
    actualSound->setPosition(0);
    actualSound->play();
    lastPlayedSoundId=mySetSoundId;
}






int SoundObjectBase::getLastPlayedSoundId(){
    return lastPlayedSoundId;
}

void SoundObjectBase::pausePlay(){
    
    for (int i=0;i<sounds.size();i++){
        sounds[i]->setPaused(true);
    }
   

    
    for (int i=0;i<alSounds.size();i++){
        alSounds[i]->stop();
    }
   // actualSound->stop();
 //   alSounds[actualSoundId]->stop();
    //sounds[actualSoundId]->setPaused(true);
}


/*
float SoundObjectBase::fade(){
    float f=actualFade;
    if(isfadeing){
        if(abs(fadeTarget-actualFade)>fadeamount){
            f=actualFade+fadeamount;
        }else{
            f=fadeTarget;
            endFade();
        }
    }
    cout<<f<<endl;
    actualFade=f;
    return f;
}*/




//--------------------------------------------------------------


void SoundObjectBase::clear() {
    if(bRegisteredEvents) {
        ofUnregisterMouseEvents(this); // disable litening to mous events.
        bRegisteredEvents = false;
    }
}






//--------------------------------------------------------------
void SoundObjectBase::touchDown(ofTouchEventArgs & touch){
   cout<<"touch inside "<<myContentId<<endl;

    //addToQueue();

}

//--------------------------------------------------------------
void SoundObjectBase::touchMoved(ofTouchEventArgs & touch){
    
}

//--------------------------------------------------------------
void SoundObjectBase::touchUp(ofTouchEventArgs & touch){
  
    
    
}

//--------------------------------------------------------------
void SoundObjectBase::touchDoubleTap(ofTouchEventArgs & touch){
  //  cout<<mySound.getIsPlaying()<<endl;
    /*if(mySound.getIsPlaying()){
        mySound.stop();
    }else{
            mySound.play();
        }
    */
   // addToQueue();
    
     removeFromQueue();
    
}

//--------------------------------------------------------------
void SoundObjectBase::touchCancelled(ofTouchEventArgs & touch){
    
}


void SoundObjectBase::mouseMoved(ofMouseEventArgs & args){}
void SoundObjectBase::mouseDragged(ofMouseEventArgs & args){}
void SoundObjectBase::mousePressed(ofMouseEventArgs & args){}
void SoundObjectBase::mouseReleased(ofMouseEventArgs & args){
 /*   ofVec2f mousePos = ofVec2f(args.x, args.y);

    ofNotifyEvent(clickedInsideGlobal, mousePos);
*/
    /*if (inside(args.x, args.y)) {
        // if the mouse is pressed over the circle an event will be notified (broadcasted)
        // the circleEvent object will contain the mouse position, so this values are accesible to any class that is listening.
        ofVec2f mousePos = ofVec2f(args.x, args.y);
        ofNotifyEvent(clickedInsideGlobal, mousePos);
    }*/
}




void SoundObjectBase::setColor(ofColor color){
    myColor=color;
    
    myPlayColor.set(myColor.r,myColor.g,myColor.b,200);
    myStopColor.set(myColor.r,myColor.g,myColor.b,100);
    
}




float SoundObjectBase::fadeOut(){

}

float SoundObjectBase::fadeIn(){
}



void SoundObjectBase::setMute(bool _mute){
    bismute=_mute;

}


bool SoundObjectBase::getMute(){
    return bismute;
}

bool SoundObjectBase::getIsInside(){
    return isInside;

}



void SoundObjectBase::setIsBaseBeat(bool _isBase){
    bIsBase= _isBase;
}

void SoundObjectBase::drawArcStrip(float percent, ofVec2f center, float radius)
{
    float theta = ofMap(percent, 0, 1, 0, 360.0, true);
    
    float outerRadius=radius;
    float innerRadius=5;
    ofPushMatrix();
    //  ofTranslate(rect->getX(),rect->getY());
    
    ofBeginShape();
    
    {
        float x = sin(-ofDegToRad(0));
        float y = cos(-ofDegToRad(0));
        ofVertex(center.x+outerRadius*x,center.y+outerRadius*y);
    }
    
    for(int i = 0; i <= theta; i+=10)
    {
        float x = sin(-ofDegToRad(i));
        float y = cos(-ofDegToRad(i));
        
        ofVertex(center.x+outerRadius*x,center.y+outerRadius*y);
    }
    
    {
        float x = sin(-ofDegToRad(theta));
        float y = cos(-ofDegToRad(theta));
        ofVertex(center.x+outerRadius*x,center.y+outerRadius*y);
        ofVertex(center.x+innerRadius*x,center.y+innerRadius*y);
    }
    
    for(int i = theta; i >= 0; i-=10)
    {
        float x = sin(-ofDegToRad(i));
        float y = cos(-ofDegToRad(i));
        
        ofVertex(center.x+innerRadius*x,center.y+innerRadius*y);
    }
    
    {
        float x = sin(-ofDegToRad(0));
        float y = cos(-ofDegToRad(0));
        ofVertex(center.x+innerRadius*x,center.y+innerRadius*y);
    }
    
    ofEndShape();
    ofPopMatrix();
}


void SoundObjectBase::setScreenpos(ofVec2f _pos){
    screenpos.set(_pos);
}



/*
//--------------------------------------------------------------
void SoundObjectBase::guiEvent(ofxUIEventArgs &e)
{
    
    
    if(!e.widget->getName().compare("POSITION"))
    {
        //ofxUIButton *button = (ofxUIButton *) e.widget;
     //   loadUrl();
        //cout<<"button"<<endl;
        
        ofxUIRotarySlider *slider =(ofxUIRotarySlider*)e.widget;
        mySound.setPosition(slider->getScaledValue());
       
        
    }
    
    
    if(!e.widget->getName().compare("STOPP"))
    {
        cout<<"button!"<<endl;
        
        ofxUIImageButton *button = (ofxUIImageButton *) e.widget;
        bool isDown= button->getValue();
        
        /*   if(isDown){
         for(int i=0;i<numberOfSounds;i++){
         if( soundPoints[i].closest){
         cout<<soundPoints[i].isplaying<<endl;
         if(soundPoints[i].isplaying){
         //                   sounds[i].stop();
         soundPoints[i].isplaying=false;
         cout<<"sounds "<<i<<" stopped"<<endl;
         }else{
         //                 sounds[i].play();
         soundPoints[i].isplaying=true;
         cout<<"sounds "<<i<<" start"<<endl;
         
         }
         }
         }
         }*/
        
        
        /*
         ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
         bool val = toggle->getValue();
         
         if(val){
         
         for(int i=0;i<SOUNDSSIZE;i++){
         if( soundPoints[i].closest){
         sounds[i].stop();
         cout<<"sounds "<<i<<" stopped"<<endl;
         }
         }
         
         }else {
         for(int i=0;i<SOUNDSSIZE;i++){
         if( soundPoints[i].closest){
         sounds[i].play();
         cout<<"sounds "<<i<<" started"<<endl;
         }
         }
         
         }*/
 //   }
    
    
//}


/*

float SoundObjectBase::getDirection(float lat1,float lon1,float lat2, float lon2){
 
    float dLat = ofDegToRad(lat2-lat1);
    float dLon = ofDegToRad(lon2-lon1);
    lat1 = ofDegToRad(lat1);
    lat2 = ofDegToRad(lat2);
    
    float y = sin(dLon) * cos(lat2);
    float x = cos(lat1)*sin(lat2)-sin(lat1)*cos(lat2)*cos(dLon);
    float brng = ofRadToDeg(atan2(y, x));
    
    return brng;
    
}*/

//--------------------------------------------------------------
/*

float SoundObjectBase::getDistance(float lat1,float lon1,float lat2, float lon2){
    
    int R = 6371; // km
    float dLat = ofDegToRad(lat2-lat1);
    float dLon = ofDegToRad(lon2-lon1);
    lat1 = ofDegToRad(lat1);
    lat2 = ofDegToRad(lat2);
    
    float a = sin(dLat/2) * sin(dLat/2)+sin(dLon/2) * sin(dLon/2) * cos(lat1) * cos(lat2); 
    float c = 2 * atan2(sqrt(a), sqrt(1-a)); 
    float d = R * c;
    return d;
}*/