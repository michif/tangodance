//
//  soundObject.h
//  GeosoundsMarie
//
//  Created by Michael Flueckiger on 3.3.2014
//  Copyright (c) 2014 Michael Flückiger. All rights reserved.
//


#ifndef ColorWindows_ScreenSaverController_h
#define ColorWindows_ScreenSaverController_h

#include "ofMain.h"
#include "Poco/RegularExpression.h"
#include "ofxOpenALSoundPlayer.h"



/*
 struct soundPoint{
 ofVec2f myPosition;
 float distance;
 float direction;
 string myname;
 int myId;
 bool closest;
 bool isplaying;
 
 };*/


class SoundObject{
	
public:
    
    SoundObject ();
    ~SoundObject ();
    
    
    static ofEvent<int> clickedInsideGlobal;
    static ofEvent<int> stepOverThreshold;
    static ofEvent<int> stepOutThreshold;
    
    
    static ofEvent<int> tick;

    
    
    
    bool isInside;
    
    bool getIsInside();
    
    void setup();
    void update();
    void draw();
    void clear();
    
    
    
    //We need to declare all this mouse events methods to be able to listen to mouse events.
    //All this must be declared even if we are just going to use only one of this methods.
    void mouseMoved(ofMouseEventArgs & args);
    void mouseDragged(ofMouseEventArgs & args);
    void mousePressed(ofMouseEventArgs & args);
    void mouseReleased(ofMouseEventArgs & args);
    
    
    void touchDown(ofTouchEventArgs & touch);
    void touchMoved(ofTouchEventArgs & touch);
    void touchUp(ofTouchEventArgs & touch);
    void touchDoubleTap(ofTouchEventArgs & touch);
    void touchCancelled(ofTouchEventArgs & touch);
    
    
    
    
    void setSound(string fileName);
    void setTransposedSound(string fileName);
    
    void setBreak(string fileName);
    void setParallel(string fileName);
    void setParallelBreak(string fileName);




    
    void setIndex(int _index);
    int getIndex();
    int myIndex;
    
    string myTitle;
    string mySoundfile;
    
    void setTitle(string _title);
    string getTitle();
    
    void setSoundfile(string _soundfile);
    void setSoundPath(string _soundfile);

    string getSoundfile();
    
    void setLoop(bool _loop);
    void setRemainInCircle(bool _remain);
    bool remain;
    
    
    void setColor(ofColor color);
    ofColor myColor;
    ofColor myPlayColor;
    ofColor myQueueColor;
    ofColor myStopColor;
    
    
    vector<ofxOpenALSoundPlayer*> alSounds;
    vector<ofxOpenALSoundPlayer*> parallels;

    vector<ofxOpenALSoundPlayer*> breaks;
    vector<ofxOpenALSoundPlayer*> parallelbreaks;

    ofxOpenALSoundPlayer * actualSound;

        vector<string>soundpath;
    
    int actualSoundId=0;
    int mySetSoundId=0;
    int lastPlayedSoundId=0;

    void setActualSoundId(int _id);
    int getActualSoundId();

    void setActualVariationOffset(int _offset);
    int getActualVariationOffset();
    
    void setVariationJump(int _jump);
    int variationJump;
    
    int getLastPlayedSoundId();
    
    
    
    float originalVolume;
    
    /* Not yet implemented I belive...*/
    float fadeOut();
    float fadeIn();
    float fade();
    float fadespeed;
    float fadeTo(float fadeTarget,float _actualFade);
    float soundfileFade();
    void startFade(float _target);
    void startFade(float _target, float _actualFade);
    void endFade();
    void setFadeTarget(float _target);
    float fadeTarget;
    float soundFadeTarget;
    float actualFade;
    float fadeamount;
    void setFadeSpeed(float _speed);

    
    //not impelmented...
    void setMute(bool _mute);
    bool getMute();
    
    
    
    void setMinDistMute(float _mindist);
    float getMinDistMute();
   
    void drawArcStrip(float percent, ofVec2f center, float radius);

    void addToQueue();
    void removeFromQueue();
    
    void setIsAllowedHalf(bool _isAllowedHalf);
    bool getIsAllowedHalf();
    
    
    void setIsBreak(bool _isbreak);
    bool getIsBreak();
    void unsetIsBreak();
    bool bIsBreak;

    
    void setIsParallel(bool _isbreak);
    bool getIsParallel();
    void unsetIsParallel();
    bool bIsParallel;
    
    void instantPlay();
    void instantReplay();

    void onTick(int & e);
    void onStart(int & e);
    void onHalf(int &e);
    void onStop(int & e);

    void switchSound(int _id);
    
    
    void resetPlay();
    void switchPlay();
    void startPlayAt(float _pos);
    
    void setStartFromPos(bool _startFromPos);
    bool getStartFromPos();

    void pausePlay();

    
    ofVec2f screenpos;
   void setScreenpos(ofVec2f _pos);
int height;
    int width;

    
    float getMySoundLength();
    bool getIsLoaded();
    
    
    void setIsBaseBeat(bool _bIsBase);

    
    void setBlock(int _ticks);
    
    
    
    //MovingObject fadeObject;
    
    void setSleepTime(float _t);
    float getSleepTime();
    bool getIsSleeping();
    
    bool dontStopOnTick=false;
    
    
    float fadeTo(float _fadeTarget, float _actualfade, float _fadespeed);
    float actualSpeed;
    float speedTarget;
    
    
    void setVolume(float vol);
    
    
private:
    
    
    bool bIsAllowedHalf;
    bool bIsStartFromPos; //Start from given pos
    
    float startTime; // store when we start playing

    float mySleepDelay;
    float bIsBase;

    bool bIsPlaying=false;
    
    bool bHasVariations=false;
    bool bHasBreaks=false;
    bool bHasParallels=false;
    bool bHasParallelBreaks=false;
    
    
    float muteVolume;
    bool bismute;
    bool isfadeing;
    bool bRegisteredEvents;
    float heading;
    float soundvolume;
    float soundpos;

    
    bool snapToGrid;
    bool isAddedToQueue;
    
    bool bIsSleeping;

    
    float mySoundLength;
    
    
    bool bIsLoaded;
    bool bTransposedIsLoaded;
    
    
    
    float actualVariationOffset; //-> Zum hin und her Switchen der Päckchen...
    
};

#endif
