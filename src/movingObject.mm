//
//  movingObject.cpp
//  Tangodance
//
//  Created by Michael Flueckiger on 14.09.14.
//
//

#include "movingObject.h"




MovingObject :: MovingObject ()
{
    cout << "creating MovingObject" << endl;
 

}

MovingObject :: ~MovingObject ()
{
    cout << "destroying MovingObject" << endl;
    clear();
    
}

//--------------------------------------------------------------
void MovingObject::setup(){
    speed.set(0,0,0);
    drag=0.98;
    position.set(0,0,0);
    maxSpeed=5;
    bIsMoving=false;
    
}


//--------------------------------------------------------------
void MovingObject::update(){
    move();

}


//--------------------------------------------------------------
void MovingObject::draw(){
    ofPushMatrix();
    ofSetColor(255,0, 0, 0);
    ofTranslate(position);
    ofRect(0, 0, 20, 20);
    ofPopMatrix();
    
}


void MovingObject::startMove(){
    bIsMoving=true;
};
void MovingObject::endMove(){
    bIsMoving=false;
};
void MovingObject::moveTo(ofVec3f(_target)){
    setTarget(_target);
    bIsMoving=true;
};

//--------------------------------------------------------------
void MovingObject::move(){
    if(bIsMoving){

        ofVec3f accel;
        ofVec3f distance;
        
        if(bAccelerated){
            distance.set(target);
            distance-=(position);
            distance.limit(maxSpeed);
            accel.set(distance);
            
            speed+=(accel);
            speed*=(drag);
            
            position+=speed;
            if(distance.length()<0.0001 && speed.length()<0.0001){
                position.set(target);
            }
        }
        
        else{
            distance.set(target);
            distance-=(position);
            speed.set(distance);
            cout<<
            speed.limit(maxSpeed);
            position+=speed;
            if(distance.length()<0.0001 && speed.length()<0.0001){
                position.set(target);
            }
        }
    }
}


void MovingObject::clear(){

}


//--------------------------------------------------------------

void MovingObject::setTarget(ofVec3f(newTarget)){
    
};
//--------------------------------------------------------------

ofVec3f MovingObject::getTarget(){
    return target;
};
//--------------------------------------------------------------

void MovingObject::setPosition(ofVec3f(newposition)){
    position.set(newposition);
};
//--------------------------------------------------------------

ofVec3f MovingObject::getPosition(){
    return position;
};
//--------------------------------------------------------------

void MovingObject::setMaxSpeed(float _maxSpeed){
    maxSpeed=_maxSpeed;
};
//--------------------------------------------------------------

float MovingObject::getMaxSpeed(){
    return maxSpeed;
};
//--------------------------------------------------------------

void MovingObject::setAccelerated(bool acc){
    bAccelerated=acc;
};
//--------------------------------------------------------------

bool MovingObject::getAccelerated(){
    return bAccelerated;
};


