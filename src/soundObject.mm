//
//  soundcontroller.cpp
//  Geosounds
//
//  Created by Michael Flueckiger on 16.04.12.
//  Copyright (c) 2014 Michael Flückiger. All rights reserved.
//

#include "soundObject.h"
#include "soundcontroller.h"



float origfadeamount=0.01;
float scalefact=800;

SoundObject :: SoundObject ()
{
    bIsBase=false;
    cout << "creating SoundObject" << endl;
    originalVolume=1;
    fadeamount=0.1;
    actualFade=0;
    isfadeing=false;
    fadespeed=0.05;
    bismute=false;
    muteVolume=0.1;
    fadeTarget=1;

    bRegisteredEvents = false;
    
    snapToGrid=true;
    isAddedToQueue=false;
    screenpos.set(0,0);
    
    height=200;
    width=10;
    remain=false;

    actualSoundId=0;
    
bIsLoaded=false;
    bIsBreak=false;
    
    mySleepDelay=-1;
    bIsSleeping=false;

    bIsAllowedHalf=false;
    speedTarget=1;
    actualSpeed=1;
    
}

SoundObject :: ~SoundObject ()
{
    cout << "destroying SoundObject" << endl;
    clear();

}



//--------------------------------------------------------------
void SoundObject::setup(){
    if(bIsBase)height=50;

    
    soundvolume=1;
    if(bIsBase)soundvolume=0.9;
    actualSoundId=0;
    
    if(!bRegisteredEvents) {
        ofRegisterMouseEvents(this); // this will enable our circle class to listen to the mouse events.
        ofRegisterTouchEvents(this); // this will enable our circle class to listen to the mouse events.
        bRegisteredEvents = true;
    }
    

    
 //  ofAddListener(SC->tick , this, &SoundObject::onTick);
    //listening to this event will enable us to get events from any instance of the circle class as this event is static (shared by all instances of the same class).

    ofAddListener(SC->cycleEnded,this,&SoundObject::onTick);
    ofAddListener(SC->cycleStart,this,&SoundObject::onStart);
    ofAddListener(SC->cicleHalf,this,&SoundObject::onHalf);
    ofAddListener(SC->cycleStop,this,&SoundObject::onStop);

    
    
    actualSound=alSounds[0];
    mySleepDelay=1;

    
    //bIsLoaded=false;
 //   fadeObject.setup();

   /* fadeObject.setTarget(ofVec3f(100,0,0));
    fadeObject.setMaxSpeed(5);
    fadeObject.setAccelerated(false);
    fadeObject.setPosition(ofVec3f(0,0,0));
*/
  }



//--------------------------------------------------------------
void SoundObject::onTick(int & e){
    //  cout<<"it Ticked "<<e<<endl;
    if(isAddedToQueue){
       // resetPlay();
       // removeFromQueue();
    }
}


//--------------------------------------------------------------
void SoundObject::onHalf(int & e){
    if( !bIsAllowedHalf)return;
   // cout<<"on Half "<< isAddedToQueue<<endl;

    if(isAddedToQueue){
        if(bIsStartFromPos){
            startPlayAt(SC->getBaseCyclePos());
            
        }else{
            resetPlay();
        }
    }
    
    if(actualSound->getIsPlaying() && SC->getTickcounter()>1){
        if(bHasParallels){
            switchPlay();}
       // startPlayAt(SC->getBaseCyclePos());
    }
    
    //  cout<<"it Ticked "<<e<<endl;
    // resetPlay();
    // removeFromQueue();
   // if(actualSound->getIsPlaying()){
   // switchPlay();
   // }
    
}


//--------------------------------------------------------------
void SoundObject::onStart(int & e){
  //  cout<<"it started"<<e<<endl;
    
    
    
    if(isAddedToQueue){
        resetPlay();
        removeFromQueue();
    }
    
}

//--------------------------------------------------------------
void SoundObject::onStop(int & e){
    
    if(actualSound->getIsPlaying() && !getIsBreak()){
    //pausePlay();
    speedTarget=0;
    }
    removeFromQueue();
}

//--------------------------------------------------------------
void SoundObject::update(){
    actualSpeed=fadeTo(speedTarget, actualSpeed, 0.1);
    if(bIsBase)actualSound->setVolume(0.82);
       actualSound->setVolume(soundvolume);

    
    actualSound->setSpeed(actualSpeed);
    if(actualSpeed==0){
        actualSound->stop();
    }
    
    float timer = ofGetElapsedTimeMillis() - startTime;
    if(timer>mySleepDelay){
        bIsSleeping=false;
    }
    
   
   
    
   }


//--------------------------------------------------------------
void SoundObject::draw(){
    if((bIsSleeping&&!actualSound->getIsPlaying())|| bIsBase)return;
    
     ofPushMatrix();
     ofTranslate(screenpos.x,screenpos.y);
    
    if(!isAddedToQueue && !actualSound->getIsPlaying()){
        ofSetColor(myQueueColor);
        ofRect(0,0,width,height);
    }
    
    if(isAddedToQueue && !actualSound->getIsPlaying()){
        ofSetLineWidth(1);
     ofSetColor(myPlayColor);
     ofRect(0,0,width,height);
     }
     
     if(actualSound->getIsPlaying()){
     ofSetColor(myPlayColor);
     ofRect(0,ofMap(actualSound->getPosition(), 0, 1,0,height),width,ofMap(actualSound->getPosition(), 0, 1, height, 0));
     }
     ofPopMatrix();
    
    
  /*  ofPushMatrix();
    ofTranslate(screenpos.x,screenpos.y);
    if(isAddedToQueue){
            ofSetColor(myQueueColor);
            ofRect(0,0,height,height);
        }
        
        if(actualSound->getIsPlaying()){
            ofSetColor(myPlayColor);
            ofRect(0,0,height,height);
        }
    ofPopMatrix();*/
}

//--------------------------------------------------------------

void SoundObject::setFadeSpeed(float _speed){
    fadespeed=_speed;
    
}




//--------------------------------------------------------------
/*void SoundObject::loadSound(string fileName){
    mySound.loadSound(fileName);
}


void SoundObject::loadSoundFileById( int _id){
    string mypath="sounds/";
    string fileName=soundpath[_id];
    mypath+=fileName;
    mySound.loadSound(ofToDataPath(mypath,true));
}*/


void SoundObject::setSoundfile(string _soundfile){
    mySoundfile=_soundfile;
   // soundpath.push_back(_soundfile);
}


string SoundObject::getSoundfile(){
    return mySoundfile;
}


void SoundObject::setSoundPath(string _soundfile){
     soundpath.push_back(_soundfile);
}


void SoundObject::setActualVariationOffset(int _offset){
    actualVariationOffset=_offset;
  actualSoundId= mySetSoundId+actualVariationOffset;
}

int SoundObject::getActualVariationOffset(){
    return actualVariationOffset;
}


void SoundObject::setVariationJump(int _jump){
    variationJump=_jump;
}

void SoundObject::setActualSoundId(int _id){
    actualSoundId = _id+actualVariationOffset;
    mySetSoundId=_id;
    //mySound.loadSound(ofToDataPath(mypath,true));
}

int SoundObject::getActualSoundId(){
    return actualSoundId;
}






void SoundObject::switchSound(int _id){
   /* actualSoundId = _id+actualVariationOffset;
    mySetSoundId=_id;
    float pos=actualSound->getPosition();
    bool isPlaying=actualSound->getIsPlaying();
    if(isPlaying){
    actualSound->stop();
    }
  //  actualSound=alSounds[actualSoundId];
    if(isPlaying){
    //actualSound->play();
    }
    //actualSound->setPosition(pos);*/
}

//--------------------------------------------------------------
void SoundObject::setSound(string fileName){
       cout<<"loading sound "<<"sounds/"+fileName<<endl;

    try
    {
        string mypath="sounds/";
        mypath+=fileName;
        
        
        cout<<"loading sound "<< ofToDataPath(mypath,true)<<endl;
        
        ofxOpenALSoundPlayer * s =new ofxOpenALSoundPlayer();
        s->loadSound(ofToDataPath(mypath,true),true);
    //    s->setMultiPlay(true);
        mySoundLength = s->length/44100/4*1000;
        cout<<"length "<<mySoundLength<<endl;

        bIsLoaded=true;
        alSounds.push_back(s);
       actualSound=alSounds[0];
    }
    catch (int e)
    {
        cout << "An exception occurred. Exception Nr. " << e << '\n';
    }
    
}


//--------------------------------------------------------------
void SoundObject::setTransposedSound(string fileName){
    cout<<"loading sound "<<"sounds/"+fileName<<endl;
    
    
    try
    {
        string mypath="sounds/";
        mypath+=fileName;
        cout<<"loading sound "<< ofToDataPath(mypath,true)<<endl;
       // myTransposedSound.loadSound(ofToDataPath(mypath,true));
        //myTransposedSound.play();
        //myTransposedSound.setPosition(0.9999);
       // float ms = myTransposedSound.getPositionMS();
        //mySoundLength=ms;
        //myTransposedSound.setPosition(0);
       // myTransposedSound.stop();
        
        
        ofxOpenALSoundPlayer * s =new ofxOpenALSoundPlayer();
        s->loadSound(ofToDataPath(mypath,true),true);
       // s->setMultiPlay(true);
        cout<<"length "<<mySoundLength<<endl;
        //SC->baseCycleDuration=mySoundLength;
        bTransposedIsLoaded=true;
        alSounds.push_back(s);
        
        bHasVariations=true;

        
        
       // sounds.insert(sounds.begin() + 1, myTransposedSound);
        //sounds.push_back(&myTransposedSound);
    }
    catch (int e)
    {
        cout << "An exception occurred. Exception Nr. " << e << '\n';
    }
    
    
    
  //  myTransposedSound.setLoop(false);
   // myTransposedSound.setVolume(0);
   // soundvolume=0;
    
}


//--------------------------------------------------------------
void SoundObject::setBreak(string fileName){
    cout<<"loading break "<<"sounds/"+fileName<<endl;
    
    
    try
    {
        string mypath="sounds/";
        mypath+=fileName;
        cout<<"loading break "<< ofToDataPath(mypath,true)<<endl;
        
        ofxOpenALSoundPlayer * s =new ofxOpenALSoundPlayer();
        s->loadSound(ofToDataPath(mypath,true),true);
       // s->setMultiPlay(true);
        mySoundLength = s->length/44100/4*1000;
        cout<<"break length "<<mySoundLength<<endl;
        breaks.push_back(s);
        bHasBreaks=true;
    }
    catch (int e)
    {
        cout << "An exception occurred. Exception Nr. " << e << '\n';
    }
    
}


//--------------------------------------------------------------
void SoundObject::setParallel(string fileName){
    cout<<"loading break "<<"sounds/"+fileName<<endl;
    
    
    try
    {
        string mypath="sounds/";
        mypath+=fileName;
        cout<<"loading parallel "<< ofToDataPath(mypath,true)<<endl;
        
        ofxOpenALSoundPlayer * s =new ofxOpenALSoundPlayer();
        s->loadSound(ofToDataPath(mypath,true),true);
        // s->setMultiPlay(true);
        mySoundLength = s->length/44100/4*1000;
        cout<<"parallel length "<<mySoundLength<<endl;
        parallels.push_back(s);
        bHasParallels=true;
    }
    catch (int e)
    {
        cout << "An exception occurred. Exception Nr. " << e << '\n';
    }
    
}


//--------------------------------------------------------------
void SoundObject::setParallelBreak(string fileName){
    cout<<"loading break "<<"sounds/"+fileName<<endl;
    try
    {
        string mypath="sounds/";
        mypath+=fileName;
        cout<<"loading parallel break"<< ofToDataPath(mypath,true)<<endl;
        
        ofxOpenALSoundPlayer * s =new ofxOpenALSoundPlayer();
        s->loadSound(ofToDataPath(mypath,true),true);
        // s->setMultiPlay(true);
        mySoundLength = s->length/44100/4*1000;
        cout<<"parallel break length "<<mySoundLength<<endl;
        parallelbreaks.push_back(s);
        bHasParallelBreaks=true;
    }
    catch (int e)
    {
        cout << "An exception occurred. Exception Nr. " << e << '\n';
    }
    
}


void SoundObject::setIsBreak(bool _isbreak){
    bIsBreak=_isbreak;
}


bool SoundObject::getIsBreak(){
    return bIsBreak;
}


void SoundObject::setIsParallel(bool _isparallel){
    bIsParallel=_isparallel;
}


bool SoundObject::getIsParallel(){
    return bIsParallel;
}

float SoundObject::getMySoundLength(){
    return mySoundLength;
}


void SoundObject::setIsAllowedHalf(bool _isAllowedHalf){
    bIsAllowedHalf=_isAllowedHalf;
}


bool SoundObject::getIsAllowedHalf(){
    return bIsAllowedHalf;
}


//--------------------------------------------------------------
void SoundObject::setRemainInCircle(bool _remain){
    remain=_remain;
}


//--------------------------------------------------------------
void SoundObject::setLoop(bool loop){
   // mySound.setLoop(loop);
}

//--------------------------------------------------------------
void SoundObject::addToQueue(){
    if(bIsBase){
        //mySoundLength = alSounds[actualSoundId]->length/44100/4*1000;
        // cout<<"set New Length  "<< mySoundLength<<endl;
      //  SC->baseCycleDuration=mySoundLength;
    }
    isAddedToQueue=true;
   // loadSoundFileById(actualSoundId);
    }

//--------------------------------------------------------------

void SoundObject::removeFromQueue(){
    isAddedToQueue=false;
}

//--------------------------------------------------------------
/*Play Sound without wating for soundcircle */
void SoundObject::instantPlay(){
    
 //   if (!isSleeping) {
        if(!actualSound->getIsPlaying()){
            cout<<"instant playing"<<actualSound->getIsPlaying()<<endl;

            isAddedToQueue=true;
            resetPlay();
            removeFromQueue();
        }
   // }
    
    
   }

//--------------------------------------------------------------
/*Play Sound without wating for soundcircle */
void SoundObject::instantReplay(){
        isAddedToQueue=true;
        resetPlay();
        removeFromQueue();
   
}



void SoundObject::setIndex(int _index){
    myIndex=_index;

}

int SoundObject::getIndex(){
    return myIndex;
}


void SoundObject::setTitle(string _title){
    myTitle=_title;
}


string SoundObject::getTitle(){
    return myTitle;
}


bool SoundObject::getIsLoaded(){
    
    return bIsLoaded;
}


void SoundObject::startFade(float _target){
    isfadeing=true;
    fadeTarget=_target;
    fadeamount=origfadeamount;
}


void SoundObject::startFade(float _target, float _actualfade){
    isfadeing=true;
    fadeTarget=_target;
    fadeamount=origfadeamount;
    actualFade=_actualfade;
}

void SoundObject::endFade(){
    isfadeing=false;
}


float SoundObject::fade(){
    if(isfadeing){
    actualFade+=fadeamount;
        if(actualFade>1 || actualFade<0)endFade();
    actualFade=ofClamp(actualFade,0,1);
    }
     return actualFade;
}

void SoundObject::setFadeTarget(float _target){
    fadeTarget=_target;
}



float SoundObject::fadeTo(float _fadeTarget, float _actualfade, float _fadespeed){
    float target =_fadeTarget;
    float actualfade=_actualfade;
    float fadespeed=_fadespeed;
    float fadeDistance = target-actualfade;
    if(abs(fadeDistance)<0.09){
        actualfade=target;
        return actualfade;
    }
    float fadefact=fadeDistance*fadespeed;
    actualfade+=fadefact;
    return actualfade;
}




float SoundObject::fadeTo(float _fadeTarget, float _actualfade){
    float target =_fadeTarget;
    float actualfade=_actualfade;
    float fadeDistance = target-actualfade;
    if(isfadeing){
    if(abs(fadeDistance)<0.01){
        actualfade=target;
        endFade();
        return actualfade;
    }
    float fadefact=fadeDistance*fadespeed;
    actualfade+=fadefact;
    }
    return actualfade;
}



void SoundObject::resetPlay(){
    if(bIsSleeping)return;
    if(!bIsBase && bIsSleeping)return;
    
    actualSound->stop();
    for (int i=0;i<alSounds.size();i++){
        alSounds[i]->stop();
    }
    
    for (int i=0;i<parallels.size();i++){
        parallels[i]->stop();
    }
    
    for (int i=0;i<parallelbreaks.size();i++){
        parallelbreaks[i]->stop();
    }
    
    for (int i=0;i<breaks.size();i++){
        breaks[i]->stop();
    }
    

    

    
   // if( actualSound->getIsPlaying()&&dontStopOnTick)return;
    int offset=0;
    if(bHasVariations){
    offset=SC->getVariationOffset();
    }
    int myActualJump= offset*variationJump;
    cout<<"offset"<<offset<<" Jump "<<myActualJump<<endl;


    int soundid=0;
    if(bHasVariations){
      soundid=SC->getChordOffset();
    }
    if(bHasBreaks){
    setIsBreak(SC->getIsBreak());
    }
    
    if(bHasParallels){
        setIsParallel(SC->getIsParallel());
    }
    
    
    if(getIsParallel() &! getIsBreak()){
        cout<<"is parallel"<<endl;
        if(bHasParallels){
        int mysound=soundid+myActualJump;
        if(mysound>parallels.size())mysound=parallels.size()-1;
            actualSound= parallels[mysound];
        }
        
    }

    
    if(getIsParallel() && getIsBreak()){
        cout<<"is parallel break"<<endl;
        SC->setNoHalfCicle(true);

        if(parallels.size()!=0 && parallelbreaks.size()!=0){
            if(offset>parallelbreaks.size()-1)offset=parallelbreaks.size()-1;
            actualSound= parallelbreaks[offset];
            setIsBreak(true);

        }
        SC->setIsBreak(false);
    }
    
    
    if(getIsBreak() &! getIsParallel()){
        cout<<"is break"<<endl;
        SC->setNoHalfCicle(true);
        if(breaks.size()>0){
            if(offset>breaks.size()-1)offset=breaks.size()-1;
            if(SC->moveenergy_slowdrag>ENERGYHIGHTRIGGER && SC->getTickcounter()>15){
                offset=(int)ofRandom(2,5.2);
            }
            actualSound= breaks[offset];
            setIsBreak(true);

        }
        SC->setIsBreak(false);
    }
    
    if(!getIsBreak() &! getIsParallel()){
        int mysound=soundid+myActualJump;
        if(mysound>alSounds.size())mysound=alSounds.size()-1;
        actualSound= alSounds[mysound];
    }
    
    
  
    if(SC->getTickcounter()>1)actualSpeed=1;
    
    speedTarget=1;
    startTime=ofGetElapsedTimeMillis();
    bIsSleeping=true;
    actualSound->setPosition(0);
    actualSound->play();
    if(bIsBase)lastPlayedSoundId=SC->getChordOffset();//mySetSoundId;
    
}




void SoundObject::switchPlay(){
    if(bIsSleeping)return;

    if(!bIsBase && bIsSleeping && speedTarget!=0)return;
    if(getIsBreak())return;
    
    if(SC->getIsBreak())return;
    float pos= actualSound->getPosition();//SC->getBaseCyclePos();//actualSound->getPosition();//SC->getBaseCyclePos();//;
    actualSound->stop();
    for (int i=0;i<alSounds.size();i++){
        alSounds[i]->stop();
    }
    
    for (int i=0;i<parallels.size();i++){
        parallels[i]->stop();
    }
    
    for (int i=0;i<parallelbreaks.size();i++){
        parallelbreaks[i]->stop();
    }
    
    for (int i=0;i<breaks.size();i++){
        breaks[i]->stop();
    }

    
   
    int offset=0;
    
    if(bHasVariations){
        offset=SC->getVariationOffset();
    }
    int myActualJump= offset*variationJump;
    
    int soundid=0;
    if(bHasVariations){
        soundid=SC->getChordOffset();
    }
    if(bHasBreaks){
        setIsBreak(SC->getIsBreak());
    }
    
    if(bHasParallels){
        setIsParallel(SC->getIsParallel());
    }
    

    

    
    if(getIsParallel() &! getIsBreak()){
        cout<<"is parallel"<<endl;
        int mysound=soundid+myActualJump;
        if(mysound>parallels.size())mysound=parallels.size()-1;
        actualSound= parallels[mysound];
        
    }
    
    
    if(getIsParallel() && getIsBreak()){
        cout<<"is parallel break"<<endl;
        if(parallels.size()!=0 && parallelbreaks.size()!=0){
            if(offset>parallelbreaks.size()-1)offset=parallelbreaks.size()-1;
            actualSound= parallelbreaks[offset];
        }
        setIsBreak(false);
        SC->setIsBreak(false);
    }
    
    
    if(getIsBreak() &! getIsParallel()){
        cout<<"is break"<<endl;
        if(breaks.size()!=0){
            if(offset>breaks.size()-1)offset=breaks.size()-1;
            actualSound= breaks[offset];
        }
        setIsBreak(false);
        SC->setIsBreak(false);
    }
    
    if(!getIsBreak() &! getIsParallel()){
        int mysound=soundid+myActualJump;
        if(mysound>alSounds.size())mysound=alSounds.size()-1;
        actualSound= alSounds[mysound];
    }
    
    
    
    speedTarget=1;
    actualSound->play();
    actualSound->setPosition(pos);
    startTime=ofGetElapsedTimeMillis();
    bIsSleeping=true;
    if(bIsBase)lastPlayedSoundId=SC->getChordOffset();//mySetSoundId;
    
}


void SoundObject::startPlayAt(float _pos){
    if(bIsSleeping)return;

    if(!bIsBase && bIsSleeping)return;
    if(getIsBreak())return;
    // if( actualSound->getIsPlaying()&&dontStopOnTick)return;
    actualSound->stop();

    float pos=_pos;
    
    int offset=0;
    if(bHasVariations){
        offset=SC->getVariationOffset();
    }
    int myActualJump= offset*variationJump;
    
    int soundid=0;
    if(bHasVariations){
        soundid=SC->getChordOffset();
    }
    if(bHasBreaks){
        setIsBreak(SC->getIsBreak());
    }
    if(bHasParallels){
        setIsParallel(SC->getIsParallel());
    }
    
    
    if(getIsParallel() &! getIsBreak()){
        cout<<"is parallel"<<endl;
        if(parallels.size()!=0){
        int mysound=soundid+myActualJump;
        if(mysound>parallels.size())mysound=parallels.size()-1;
       
        actualSound= parallels[mysound];
        }
    }
    
    
    if(getIsParallel() && getIsBreak()){
        cout<<"is parallel break"<<endl;
        if(parallels.size()!=0 && parallelbreaks.size()!=0){
            if(offset>parallelbreaks.size()-1)offset=parallelbreaks.size()-1;
            actualSound= parallelbreaks[offset];
        }
        setIsBreak(false);
        SC->setIsBreak(false);
    }
    
    
    if(getIsBreak() &! getIsParallel()){
        cout<<"is break"<<endl;
        if(breaks.size()!=0){
            if(offset>breaks.size()-1)offset=breaks.size()-1;
            actualSound= breaks[offset];
        }
        setIsBreak(false);
        SC->setIsBreak(false);
    }
    
    if(!getIsBreak() &! getIsParallel()){
        int mysound=soundid+myActualJump;
        if(mysound>alSounds.size())mysound=alSounds.size()-1;
        actualSound= alSounds[mysound];
    }
    
    
    //add check if length not shorter than pos!
    actualSound->play();
    actualSound->setPosition(pos);
    startTime=ofGetElapsedTimeMillis();
    bIsSleeping=true;
    
    if(bIsBase)lastPlayedSoundId=SC->getChordOffset();//mySetSoundId;
    
    
    
}







int SoundObject::getLastPlayedSoundId(){
    return lastPlayedSoundId;
}

void SoundObject::pausePlay(){
   actualSound->stop();
    
   /* for (int i=0;i<alSounds.size();i++){
        alSounds[i]->stop();
    }
    
    for (int i=0;i<parallels.size();i++){
        parallels[i]->stop();
    }
    
    for (int i=0;i<parallelbreaks.size();i++){
        parallelbreaks[i]->stop();
    }
    
    for (int i=0;i<breaks.size();i++){
        breaks[i]->stop();
    }
    
*/
    

    for (int i=0;i<alSounds.size();i++){
        //alSounds[i]->getIsPlaying();
      //  alSounds[i]->stop();
       // cout<<"set Stop "<<i<<alSounds[i]->getIsPlaying()<<endl;

    }
    
    for (int i=0;i<breaks.size();i++){
        //breaks[i]->stop();
    }

}


/*
float SoundObject::fade(){
    float f=actualFade;
    if(isfadeing){
        if(abs(fadeTarget-actualFade)>fadeamount){
            f=actualFade+fadeamount;
        }else{
            f=fadeTarget;
            endFade();
        }
    }
    cout<<f<<endl;
    actualFade=f;
    return f;
}*/




//--------------------------------------------------------------


void SoundObject::clear() {
    if(bRegisteredEvents) {
        ofUnregisterMouseEvents(this); // disable litening to mous events.
        bRegisteredEvents = false;
    }
}






//--------------------------------------------------------------
void SoundObject::touchDown(ofTouchEventArgs & touch){

}

//--------------------------------------------------------------
void SoundObject::touchMoved(ofTouchEventArgs & touch){
    
}

//--------------------------------------------------------------
void SoundObject::touchUp(ofTouchEventArgs & touch){
  
    
    
}

//--------------------------------------------------------------
void SoundObject::touchDoubleTap(ofTouchEventArgs & touch){
  //  cout<<mySound.getIsPlaying()<<endl;
    /*if(mySound.getIsPlaying()){
        mySound.stop();
    }else{
            mySound.play();
        }
    */
   // addToQueue();
    
     removeFromQueue();
    
}

//--------------------------------------------------------------
void SoundObject::touchCancelled(ofTouchEventArgs & touch){
    
}


void SoundObject::mouseMoved(ofMouseEventArgs & args){}
void SoundObject::mouseDragged(ofMouseEventArgs & args){}
void SoundObject::mousePressed(ofMouseEventArgs & args){}
void SoundObject::mouseReleased(ofMouseEventArgs & args){
 /*   ofVec2f mousePos = ofVec2f(args.x, args.y);

    ofNotifyEvent(clickedInsideGlobal, mousePos);
*/
    /*if (inside(args.x, args.y)) {
        // if the mouse is pressed over the circle an event will be notified (broadcasted)
        // the circleEvent object will contain the mouse position, so this values are accesible to any class that is listening.
        ofVec2f mousePos = ofVec2f(args.x, args.y);
        ofNotifyEvent(clickedInsideGlobal, mousePos);
    }*/
}




void SoundObject::setColor(ofColor color){
    myColor=color;
    
    myPlayColor.set(myColor.r,myColor.g,myColor.b,255);
    myQueueColor.set(myColor.r,myColor.g,myColor.b,100);
    myStopColor.set(myColor.r,myColor.g,myColor.b);
    
}




float SoundObject::fadeOut(){

}

float SoundObject::fadeIn(){
}



void SoundObject::setMute(bool _mute){
    bismute=_mute;

}


bool SoundObject::getMute(){
    return bismute;
}

bool SoundObject::getIsInside(){
    return isInside;

}



void SoundObject::setIsBaseBeat(bool _isBase){
    bIsBase= _isBase;
}


void SoundObject::setSleepTime(float _t){
    mySleepDelay=_t;
}

float SoundObject::getSleepTime(){
    return mySleepDelay;
}

bool SoundObject::getIsSleeping(){
    return bIsSleeping;
}


void SoundObject::drawArcStrip(float percent, ofVec2f center, float radius)
{
    float theta = ofMap(percent, 0, 1, 0, 360.0, true);
    
    float outerRadius=radius;
    float innerRadius=5;
    ofPushMatrix();
    //  ofTranslate(rect->getX(),rect->getY());
    
    ofBeginShape();
    
    {
        float x = sin(-ofDegToRad(0));
        float y = cos(-ofDegToRad(0));
        ofVertex(center.x+outerRadius*x,center.y+outerRadius*y);
    }
    
    for(int i = 0; i <= theta; i+=10)
    {
        float x = sin(-ofDegToRad(i));
        float y = cos(-ofDegToRad(i));
        
        ofVertex(center.x+outerRadius*x,center.y+outerRadius*y);
    }
    
    {
        float x = sin(-ofDegToRad(theta));
        float y = cos(-ofDegToRad(theta));
        ofVertex(center.x+outerRadius*x,center.y+outerRadius*y);
        ofVertex(center.x+innerRadius*x,center.y+innerRadius*y);
    }
    
    for(int i = theta; i >= 0; i-=10)
    {
        float x = sin(-ofDegToRad(i));
        float y = cos(-ofDegToRad(i));
        
        ofVertex(center.x+innerRadius*x,center.y+innerRadius*y);
    }
    
    {
        float x = sin(-ofDegToRad(0));
        float y = cos(-ofDegToRad(0));
        ofVertex(center.x+innerRadius*x,center.y+innerRadius*y);
    }
    
    ofEndShape();
    ofPopMatrix();
}


void SoundObject::setScreenpos(ofVec2f _pos){
    screenpos.set(_pos);
}


void SoundObject::setVolume(float _vol){
    soundvolume=_vol;
}



/*
//--------------------------------------------------------------
void SoundObject::guiEvent(ofxUIEventArgs &e)
{
    
    
    if(!e.widget->getName().compare("POSITION"))
    {
        //ofxUIButton *button = (ofxUIButton *) e.widget;
     //   loadUrl();
        //cout<<"button"<<endl;
        
        ofxUIRotarySlider *slider =(ofxUIRotarySlider*)e.widget;
        mySound.setPosition(slider->getScaledValue());
       
        
    }
    
    
    if(!e.widget->getName().compare("STOPP"))
    {
        cout<<"button!"<<endl;
        
        ofxUIImageButton *button = (ofxUIImageButton *) e.widget;
        bool isDown= button->getValue();
        
        /*   if(isDown){
         for(int i=0;i<numberOfSounds;i++){
         if( soundPoints[i].closest){
         cout<<soundPoints[i].isplaying<<endl;
         if(soundPoints[i].isplaying){
         //                   sounds[i].stop();
         soundPoints[i].isplaying=false;
         cout<<"sounds "<<i<<" stopped"<<endl;
         }else{
         //                 sounds[i].play();
         soundPoints[i].isplaying=true;
         cout<<"sounds "<<i<<" start"<<endl;
         
         }
         }
         }
         }*/
        
        
        /*
         ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
         bool val = toggle->getValue();
         
         if(val){
         
         for(int i=0;i<SOUNDSSIZE;i++){
         if( soundPoints[i].closest){
         sounds[i].stop();
         cout<<"sounds "<<i<<" stopped"<<endl;
         }
         }
         
         }else {
         for(int i=0;i<SOUNDSSIZE;i++){
         if( soundPoints[i].closest){
         sounds[i].play();
         cout<<"sounds "<<i<<" started"<<endl;
         }
         }
         
         }*/
 //   }
    
    
//}


/*

float SoundObject::getDirection(float lat1,float lon1,float lat2, float lon2){
 
    float dLat = ofDegToRad(lat2-lat1);
    float dLon = ofDegToRad(lon2-lon1);
    lat1 = ofDegToRad(lat1);
    lat2 = ofDegToRad(lat2);
    
    float y = sin(dLon) * cos(lat2);
    float x = cos(lat1)*sin(lat2)-sin(lat1)*cos(lat2)*cos(dLon);
    float brng = ofRadToDeg(atan2(y, x));
    
    return brng;
    
}*/

//--------------------------------------------------------------
/*

float SoundObject::getDistance(float lat1,float lon1,float lat2, float lon2){
    
    int R = 6371; // km
    float dLat = ofDegToRad(lat2-lat1);
    float dLon = ofDegToRad(lon2-lon1);
    lat1 = ofDegToRad(lat1);
    lat2 = ofDegToRad(lat2);
    
    float a = sin(dLat/2) * sin(dLat/2)+sin(dLon/2) * sin(dLon/2) * cos(lat1) * cos(lat2); 
    float c = 2 * atan2(sqrt(a), sqrt(1-a)); 
    float d = R * c;
    return d;
}*/