//
//  movingObject.h
//  Tangodance
//
//  Created by Michael Flueckiger on 14.09.14.
//
//

#ifndef __Tangodance__movingObject__
#define __Tangodance__movingObject__

#include <iostream>
#import "ofMain.h"


#endif /* defined(__Tangodance__movingObject__) */

class MovingObject {
    
    
	
    public:
    
    MovingObject ();
    ~MovingObject ();

    void setup();
    void update();
    void draw();
    void clear();

    
    
    void startMove();
    void endMove();
    void moveTo(ofVec3f(_target));
    
    
    void setTarget(ofVec3f(newTarget));
    ofVec3f getTarget();
    
    void setPosition(ofVec3f(newposition));
    ofVec3f getPosition();
    
    void setMaxSpeed(float maxSpeed);
    float getMaxSpeed();
    
    void setAccelerated(bool acc);
    bool getAccelerated();
    
    
    private:
    
    void move();

    float drag;
    bool bIsMoving;
    ofVec3f position;
    ofVec3f target;
    bool bAccelerated=false;
    float maxSpeed;
    ofVec3f speed;
};