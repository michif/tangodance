//
//  soundcontroller.cpp
//  Geosounds
//
//  Created by Michael Flueckiger on 16.04.12.
//  Copyright (c) 2014 Michael Flückiger. All rights reserved.
//

#ifndef GeosoundsMarie_soundcontroller_h
#define GeosoundsMarie_soundcontroller_h




#endif
#define SOUNDSSIZE 3
#define CLOSEDIST 0.005

#import "ofMain.h"
#include "soundObject.h"
#include "ofxCoreMotion.h"
#include "ofxOneDollar.h"



#define ENERGYDRAG 0.98
#define SLOWENERGYDRAG 0.99

#define ENERGYLOWYTRIGGER 20
#define ENERGYHIGHTRIGGER 40


#define HARDENERGYDRAG 0.96
#define PARALLELTICKTRIGGER 8
#define PITCHTRIGGER 1.3


//for convenience
#define SC Soundcontroller::getInstance()

struct soundPoint{
    ofVec2f myPosition;
    float distance;
    float direction;
    string myname;
    int myId;
    bool closest;
    bool isplaying;

};


class Soundcontroller {
    

	
public:
    
   // Soundcontroller ();
  //  ~Soundcontroller ();
    
static Soundcontroller* getInstance();

    
void setup();
void update();
void draw();
void setDevicePosition(ofVec2f _devicePosition);
void setHeading(float _heading);
    
    
    //We need to declare all this mouse events methods to be able to listen to mouse events.
    //All this must be declared even if we are just going to use only one of this methods.
    void mouseMoved(ofMouseEventArgs & args);
    void mouseDragged(ofMouseEventArgs & args);
    void mousePressed(ofMouseEventArgs & args);
    void mouseReleased(ofMouseEventArgs & args);
    
    
    void touchDown(ofTouchEventArgs & touch);
    void touchMoved(ofTouchEventArgs & touch);
    void touchUp(ofTouchEventArgs & touch);
    void touchDoubleTap(ofTouchEventArgs & touch);
    void touchCancelled(ofTouchEventArgs & touch);

    
    
    
void checkMuteSounds();
    

    
int soundsSize;
    


int numberOfSounds;
    vector<SoundObject *> baseBeats;

    
    
    vector<SoundObject *> baseBreaks;

    vector<SoundObject *> parallels;
    vector<SoundObject *> parallelBreaks;


    vector<SoundObject *> soundObjects;
    vector<SoundObject *> goodies;
    
    vector<SoundObject *> doublers;

    
    
      vector<ofColor> variationcol;
    
    vector<SoundObject *> vocals;
    int vocalIndex;
    
    vector<SoundObject *> instantPlayer;

    void createNewGesture();
    vector<ofVec2f> line;
    vector<ofVec2f> line2;

    vector<ofVec2f> recline;
    vector<ofVec2f> recline2;
    int frame;
    int recframebefore2;
    
    int lineframe;
    
    bool match1,match2;

    vector<ofVec2f> found_gesture;
    ofxOneDollar dollar;
    ofxOneDollar dollar2;

    
    ofxGesture* gesture;
    ofxGesture* gesture2;
    
    bool isTouched;

    int num_created_gestures;
    
    
    vector<ofVec2f> resample(int n, vector<ofVec2f> _line);
 
    
    int mode;
    string message;
    int hide_message_on;
    
    
    int deltaframe;
    int framebefore;
    
    void setActualBaseBeat(SoundObject * so);
    SoundObject * getActualBaseBeat();
    SoundObject * myActualBaseBeat;
    
    void setNextBaseBeat(SoundObject * so);
    void setNextBaseBeatByIndex(int _index);

    
    SoundObject * getNextBaseBeat();
    int getNextBaseBeatIndex();
    SoundObject * myNextBaseBeat;
    
    int myActualBaseBeatIndex;
    int myNextBaseBeatIndex;
    
    void setBaseBeatIndex(int _index);
    int getBaseBeatIndex();
    
    bool bIsParallel;
    void setIsParallel(bool _isParallel);
    bool getIsParallel();
    int parallelTickCounter;
    int parallelTickCounterdiff=4;
    
    
    bool bIsBreak;
    void setIsBreak(bool _isBreak);
    bool getIsBreak();
  
    bool bIsSpecialBreak;
    void setIsSpecialBreak(bool _isBreak);
    bool getIsSpecialBreak();
      
    
    string displaystring;
    int closestSoundPointId;
    
    float getSoundObjectByTitle(string title);

    void drawArcStrip(float percent, ofVec2f center, float radius);
  
    
    void startBaseCycle();
    void halfBaseCycle();

    void stopBaseCycle();
    float getBaseCyclePos();
    
    
    ofEvent<int> cycleStart;
    ofEvent<int> cicleHalf;

    ofEvent<int> cycleStop;
    ofEvent<int> cycleEnded;

    
    
    ofxCoreMotion coreMotion;
    float moveenergy;
    float moveenergy_slowdrag;


    float alpha ;
    vector<float> lowPass(vector<float> input, vector<float>output);
    
    vector<float> rollbuffer;
    vector<float> zbuffer;

    
    
    vector<ofVec3f> accelerationbuffer;
    ofVec3f smoothedAcceleration;
    
    ofTrueTypeFont font;

    
    ofxiPhoneCoreLocation * coreLocation;
    bool hasCompass;
    float heading;
    float pheading;

    float heading_ref;
    float heading_angle;
    float heading_angle_modulo;
    float pheading_angle_modulo;
    
    vector<float> headingbuffer;
    void resetHeadingBuffer();
    
    void resetXCounter();
    void resetYCounter();
    void resetZCounter();



    
    float delatheading;
    float pDeltaheading;
    float headingSpeed;
    float pHeadingSpeed;
    
    float heading_angle_goround;
    
    ofImage compassImg;
    
    float baseCycleDuration;
    float nextBaseCycleDuration;

    
    vector<int> soundIdHistory;

    
    int getVariationOffset();
    void setVariationOffset(int _offset);
    
    void switchVariationBaseBeat( int _offset);
    
    int getChordOffset();
    void setChordOffset(int _offset);
    float rawYaw;
    float rawPitch;
    float rawRoll;
    double quatpitch;
    
    
    float fadeTo(float _fadeTarget, float _actualfade, float _fadespeed);
    float actualFade;
    
    int getTickcounter();

    
    
    void addRandomGoodieToQueue(bool _sleep=true,int _sleeptime=500);
    void addRandomGoodieInstantPlay(bool _sleep=true,int _sleeptime=500);
    void startRandomCrescendoTicker(int _delay=2000, int _target=9);

    
    void addPianoGoodieToQueue(bool _specialsleep =true, int _sleeptime=500,bool _sleep=false);
    void addPianoGoodieToPlayAt(bool _specialsleep =true, int _sleeptime=500,bool _sleep=false);
    void startPianoCrescendoTicker(int _delay=2000, int _target=9);
    
    void addBandoneonGoodieToQueue(bool _specialsleep =true, int _sleeptime=500,bool _sleep=false);
    void addBandoneonGoodieToPlayAt(bool _specialsleep =true, int _sleeptime=500,bool _sleep=false);
    void startBandoneonCrescendoTicker(int _delay=2000, int _target=9);

   
    
    void addContrabassGoodieToQueue(bool _specialsleep =true,int _sleeptime=500,bool _sleep=false);
    void addContrabassGoodieToPlayAt(bool _specialsleep =true, int _sleeptime=500,bool _sleep=true);
    void startContrabassCrescendoTicker(int _delay=2000, int _target=9);


    void addPercussionGoodieToQueue(bool _sleep=true,int _sleeptime=500);
    void addRandomVocalsInstantPlay(bool _sleep=true,int _sleeptime=500);
    void addRandomSoundObjectInstantPlay(bool _sleep=true, int _sleeptime=300);
    
    void addRomanticDoublerToQueue(bool _sleep=true,int _sleeptime=500);
    void addDoublerToQueue(int index,bool _sleep=true,int _sleeptime=500);

    
    void addDramaBreak();


    
    
    void muteAllSpecials();
    void setNoHalfCicle(bool _bNoHalfCicle);
    
    
    bool isUpright=false;

    vector<ofColor> generateColorSwatchesFromColor(float _num, float _huerange, float _saturationrange, float _brightnessrange, ofColor _col);
    
    
    
    void resetAllTickTargets(int _targetTicks=50, int _tickdeviation=10);
  

    
    
private:
    
    
    int soundw=50;
    int soundh=50;
    
    bool bNoHalfCicle;
    
    bool isShortSleep;
    float shortSleepDelay;
    float shortSleepStartTime;
    
    bool bIsSleepingGoodie;
    float myGoodieSleepDelay;
    float myGoodiesActualSleepDelay;
    float myGoodieSleepDelayInit;
    float goodieSleepStartTime;
    
    int goodiesTickInit;
    int goodiesTickTarget;
 
    
    bool bIsSleepingPiano;
    float myPianoSleepDelay;
    float myPianoSleepDelayInit;
    float myPianoActualSleepDelay;
    float pianoSleepStartTime;
    
    int pianoTickInit;
    int pianoTickTarget;
    
    bool bIsSleepingBandoneon;
    float myBandonenonSleepDelay;
    float myBandoneonActualSleepDelay;
    float myBandonenonSleepDelayInit;
    float bandoneonSleepStartTime;
    
    int bandoneonTickInit;
    int bandoneonTickTarget;
    
    
    bool bIsSleepingContrabass;
    float myContrabassSleepDelay;
    float myContrabassActualSleepDelay;
    float myContrabassSleepDelayInit;
    float contrabassSleepStartTime;
    
    int contrabassTickInit;
    int contrabassTickTarget;
    
    
    bool bIsSleepingDouble;
    float mydoublerSleepDelay;
    float doublerSleepStartTime;
    
    bool bIsSleepingVocal;
    float myVocalSleepDelayInit;
    float myVocalSleepDelay;
    float vocalSleepStartTime;
    
    bool bIsSleepingSoundObjects;
    float mySoundobjectsSleepDelay;
    float soundObjectSleepStartTime;
    
    
    bool bIsAddedPiano;
    bool bIsAddedBandoneon;

    
    

    bool bDebugDraw=false;
    
    float maxAccelerationlength;
    float maxSmooth_roll;
    
    float maxMoveEnergy;
    float maxMoveEnergySlowdrag;
    float maxRollDif;

    float maxAccDif;

    
    int variationOffset;
    int chordOffset;

    
    ofColor rollCounterColor=ofColor(255,255,255);
    
    Soundcontroller ();
	static Soundcontroller* instance;
    
    bool bRegisteredEvents;

    
    float actualBaseCyclePos;
    float baseCyclePausePos; //store when pause is pressed
    float baseCycleRestLength; //restlength
    
    float startBaseCycleTime; // store when we start time timer
    float endBaseCycleTime; // when do want to stop the timer
    bool bCycleTimeReached;
    bool bCycleTimeReachedHalf;//when is in half of baseCircle
    
    bool bLoopBaseCycle;
    
    bool baseCycleIsRunning;
    
    int tickcounter;
    int ptickcounter;
 //   ofNode phonebody;
    
    //movement
    
    float roll;
    float mappedRoll;
    float rollbefore;
    float rollCounter;
    
    
    float smoothed_roll;
    float psmoothed_roll;
    float deltaSmoothRoll;

    float xCounter;
    float yCounter;
    
    float z2Counter;
    float smoothedX;
    
    //float zCounter;

    
    
    

    float pRoll;
    
    float yaw;
    float pYaw;
    
    float pitch;
    float pPitch;
    
    float rollenergy;
    float yawenergy;
    float pitchenergy;

    bool roll_clockwise;
    bool proll_clockwise;
    
   float rollClockwise;
    float rollInitTime;

    ofQuaternion initQuat;
    
    
    float xx,px;
    float yy,py;
    float zz,pz;
    
    
    ofColor zCounterColor;
    float smoothed_z;
    float psmoothed_z;
    float deltaSmoothZ;
    float zCounter;


    
    ofVec3f pAcceleration;
    ofVec3f acceleration;
    
    float accelerationLength;
    float pAccelerationLength;
    
    
    
    ofVec3f uacc;
    ofVec3f pUacc;
    ofVec3f dUacc;
    ofVec3f pDUacc;
    
    ofVec3f filteredUacc;
    ofVec3f pfilterdUacc;
    
    vector<ofVec3f> userAccelerationBuffer;


    
};