//
//  soundcontroller.cpp
//  Tangodance
//
//  Created by Michael Flueckiger on 16.04.12.
//  Copyright (c) 2014 Michael Flückiger. All rights reserved.
//

#include "soundcontroller.h"
#include "ofxiOSExtras.h"




Soundcontroller* Soundcontroller::instance = 0;

Soundcontroller* Soundcontroller::getInstance() {
	if (!instance) {
		instance = new Soundcontroller();
	}
	return instance;
}





Soundcontroller :: Soundcontroller ()
{
    cout << "creating Soundcontroller" << endl;
    bRegisteredEvents = false;

}

/*Soundcontroller :: ~Soundcontroller ()
{
    cout << "destroying Soundcontroller" << endl;
}*/

float rollTolerance=0.1;

//--------------------------------------------------------------
void Soundcontroller::setup(){
    
    
    ofEnableAlphaBlending();

    
    myGoodieSleepDelayInit=5000;
    myGoodieSleepDelay=2000;
    goodiesTickTarget=30;
    
   myVocalSleepDelayInit=500;
    myVocalSleepDelay=500;
    
    mySoundobjectsSleepDelay=500;
    shortSleepDelay=80;
    
    

    myPianoSleepDelayInit=10000;
    myPianoSleepDelay=10000;
    
    pianoTickInit=0;
    pianoTickTarget=30;
    
    myContrabassSleepDelayInit=10000;
    myContrabassSleepDelay=10000;
    
    contrabassTickInit=0;
    contrabassTickTarget=30;
    
    myBandonenonSleepDelayInit=10000;
    myBandonenonSleepDelay=10000;
    
    bandoneonTickInit=0;
    bandoneonTickTarget=30;
    
    mydoublerSleepDelay=3000;
    num_created_gestures = 0;

    createNewGesture();

   dollar.load(ofxiPhoneGetDocumentsDirectory()+"gestures.txt");
    dollar2.load(ofxiPhoneGetDocumentsDirectory()+"gestures2.txt");

    
   // variationcol.push_back(ofColor(17,40,56));
   // variationcol.push_back(ofColor(145,140,110));
   // variationcol.push_back(ofColor(51,48,29));
    
    variationcol.push_back(ofColor(102,0,102));
    variationcol.push_back(ofColor(102,0,153));
    variationcol.push_back(ofColor(102,0,240));

    //variationcol.push_back(ofColor(255,0,0));
    //variationcol.push_back(ofColor(255,83,13));
    //variationcol.push_back(ofColor(231,9,98));
    
    
    
    // 152,0,2
   // 255,27,14
    
    setIsBreak(false);
    bIsBreak=false;
    displaystring="";
    //----------------------------------
 //   gui = new ofxUICanvas(0,ofGetHeight(),ofGetWidth(),500);
   // gui = new ofxUICanvas(0,0,200,ofGetHeight());
    ofVec2f tempPos(ofGetWidth()/2,0);
    soundObjects.clear();
    cout<<"finished setup soundcontroller"<<endl;
    
    SoundObject * so =new SoundObject();
    so->setIndex(0);
    so->setColor(ofColor(255,0,0));
    so->setIsBaseBeat(true);

    baseBeats.push_back(so);
    

    
    so =new SoundObject();
    so->setIndex(1);
    so->setColor(ofColor(255,0,0));
    so->setIsBaseBeat(true);
    baseBreaks.push_back(so);
    
    
    so =new SoundObject();
    so->setIndex(2);
    so->setColor(ofColor(0,255,0));
    so->setIsBaseBeat(true);
    baseBeats.push_back(so);

    so =new SoundObject();
    so->setIndex(3);
    so->setIsBaseBeat(true);
    baseBeats.push_back(so);
    
    
   /* so =new SoundObject();
    so->setIndex(0);
    so->setColor(ofColor(255,255,0));
    so->setScreenpos(ofVec2f(0,0));
    so->setIsBaseBeat(true);
    parallels.push_back(so);
    
    
    so =new SoundObject();
    so->setIndex(0);
    so->setColor(ofColor(255,255,0));
    so->setScreenpos(ofVec2f(0,0));
    so->setIsBaseBeat(true);
    parallelBreaks.push_back(so);
    */

    SoundObject * so2=new SoundObject();
    soundObjects.push_back(so2);

    so2=new SoundObject();
    soundObjects.push_back(so2);
    
    so2=new SoundObject();
    soundObjects.push_back(so2);
    
    so2=new SoundObject();
    soundObjects.push_back(so2);
    
    so2=new SoundObject();
    soundObjects.push_back(so2);
   

    
    SoundObject * dbl=new SoundObject();
    dbl->setVariationJump(1);
    doublers.push_back(dbl);
    
    
    dbl=new SoundObject();
    dbl->setVariationJump(1);
    doublers.push_back(dbl);
    
    
    
    
    for(int i=0;i<15;i++){
        SoundObject * goodie=new SoundObject();
        goodie->setVariationJump(0);
        goodie->setIsAllowedHalf(true);
        goodies.push_back(goodie);
    }
    
    
    
    
    
    
    
    
    for(int i=0;i<9;i++){
    SoundObject * voc=new SoundObject();
    voc->setVariationJump(0);
    voc->setSleepTime(500);
    vocals.push_back(voc);
    }
    

    /*
    so3=new SoundObject();
    so3->setColor(ofColor(0,255,0));
    so3->setScreenpos(ofVec2f(0,100));
    goodies.push_back(so3);
    
    so3=new SoundObject();
    so3->setColor(ofColor(0,255,0));
    so3->setScreenpos(ofVec2f(0,100));
    goodies.push_back(so3);
    */
    
    baseBeats[0]->setSound("C_Basic_B-16bit.caf");
    baseBeats[0]->setTransposedSound("F_Basic_B-16bit.caf");
    baseBeats[0]->setTransposedSound("G_Basic_B-16bit.caf");

    
    baseBeats[0]->setTransposedSound("C_Moto_A-16bit.caf");
    baseBeats[0]->setTransposedSound("F_Moto_A-16bit.caf");
    baseBeats[0]->setTransposedSound("G_Moto_A-16bit.caf");

    baseBeats[0]->setTransposedSound("C_Moto_B-16bit.caf");
    baseBeats[0]->setTransposedSound("F_Moto_B-16bit.caf");
    baseBeats[0]->setTransposedSound("G_Moto_B-16bit.caf");
    
    baseBeats[0]->setParallel("Am_Basic_B-16bit.caf");
    baseBeats[0]->setParallel("Dm_Basic_B-16bit.caf");
    baseBeats[0]->setParallel("E_Basic_B-16bit.caf");
    
    baseBeats[0]->setParallel("Am_Moto_A-16bit.caf");
    baseBeats[0]->setParallel("Dm_Moto_A-16bit.caf");
    baseBeats[0]->setParallel("E_Moto_A-16bit.caf");
    
    baseBeats[0]->setParallel("Am_Moto_B-16bit.caf");
    baseBeats[0]->setParallel("Dm_Moto_B-16bit.caf");
    baseBeats[0]->setParallel("E_Moto_B-16bit.caf");
    
    baseBeats[0]->setBreak("Break_Basic_B-16bit.caf");
    baseBeats[0]->setBreak("Break_Moto_A-16bit.caf");
    baseBeats[0]->setBreak("Break_Moto_B-16bit.caf");
    baseBeats[0]->setBreak("Break_Moto_Drama-1.caf");

    baseBeats[0]->setParallelBreak("Break_Basic_B-16bit.caf");
    baseBeats[0]->setParallelBreak("Break_Moto_A-16bit.caf");
    baseBeats[0]->setParallelBreak("Break_Moto_B-16bit.caf");
    
    baseBeats[0]->setIsAllowedHalf(true);
    baseBeats[0]->setVariationJump(3);
    baseBeats[0]->setup();
    
    
    
    /*baseBreaks[0]->setSound("Break_Basic_B-16bit.caf");
    baseBreaks[0]->setTransposedSound("Break_Moto_A-16bit.caf");
    baseBreaks[0]->setTransposedSound("Break_Moto_B-16bit.caf");
    baseBreaks[0]->setVariationJump(1);
    baseBreaks[0]->setup();

    */
   /* parallels[0]->setSound("Am_Basic_B-16bit.caf");
    parallels[0]->setTransposedSound("Dm_Basic_B-16bit.caf");
    parallels[0]->setTransposedSound("E_Basic_B-16bit.caf");
    
    
    parallels[0]->setTransposedSound("Am_Moto_A-16bit.caf");
    parallels[0]->setTransposedSound("Dm_Moto_A-16bit.caf");
    parallels[0]->setTransposedSound("E_Moto_A-16bit.caf");
    
    
    parallels[0]->setTransposedSound("Am_Moto_B-16bit.caf");
    parallels[0]->setTransposedSound("Dm_Moto_B-16bit.caf");
    parallels[0]->setTransposedSound("E_Moto_B-16bit.caf");
    
    parallels[0]->setIsAllowedHalf(true);

    
    parallels[0]->setVariationJump(3);
    parallels[0]->setup();*/
    
   /*
    parallelBreaks[0]->setSound("Break_Basic_B-16bit.caf");
    parallelBreaks[0]->setTransposedSound("Break_Moto_A-16bit.caf");
    parallelBreaks[0]->setTransposedSound("Break_Moto_B-16bit.caf");
    parallelBreaks[0]->setVariationJump(1);
    parallelBreaks[0]->setup();
    */
    
    baseBeats[1]->setSound("C_Polka_A.caf");
    baseBeats[1]->setTransposedSound("F_Polka_A.caf");
    baseBeats[1]->setTransposedSound("G_Polka_A.caf");
    baseBeats[1]->setVariationJump(0);

    baseBeats[1]->setup();
    
    
    


    
    baseBeats[2]->setSound("C_Valse.caf");
    baseBeats[2]->setTransposedSound("F_Valse.caf");
    baseBeats[2]->setTransposedSound("G_Valse.caf");
    baseBeats[2]->setVariationJump(0);
    baseBeats[2]->setup();
    
    
    
    for(int i=0; i<baseBeats.size(); i++){
        baseBeats[i]->setColor(ofColor(ofRandom(255),ofRandom(255),ofRandom(255)));
        baseBeats[i]->setScreenpos(ofVec2f(0+i*50,0));
    }

    
    
    int soundscreenheight=1;
    int soundXpos=0;
    vector<ofColor> c;
    c=generateColorSwatchesFromColor(5,1,1,1,ofColor(0,0,255));
    
    doublers[0]->setSound("Goodie_Romance_C.caf");
    doublers[0]->setTransposedSound("Goodie_Romance_F.caf");
    doublers[0]->setTransposedSound("Goodie_Romance_G.caf");
    
    doublers[0]->setParallel("Goodie_Romance_Am.caf");
    doublers[0]->setParallel("Goodie_Romance_Dm.caf");
    doublers[0]->setParallel("Goodie_Romance_E.caf");
    
    doublers[0]->setVariationJump(0);
    doublers[0]->setScreenpos(ofVec2f(soundXpos,soundscreenheight*soundh));
    doublers[0]->setColor(c[0]);

    doublers[0]->setup();
    
    
    
    doublers[1]->setSound("Pianodouble_C.caf");
    doublers[1]->setTransposedSound("Pianodouble_F.caf");
    doublers[1]->setTransposedSound("Pianodouble_G.caf");
    
    doublers[1]->setParallel("Pianodouble_Am.caf");
    doublers[1]->setParallel("Pianodouble_Dm.caf");
    doublers[1]->setParallel("Pianodouble_E.caf");
    
    doublers[1]->setVariationJump(0);
    soundXpos+=soundw;
    doublers[1]->setColor(c[0]);

    doublers[1]->setScreenpos(ofVec2f(soundXpos,soundscreenheight*soundh));
    doublers[1]->setup();
    
    
    
    
    
    
  
    
    
    soundObjects[0]->setSound("Goodie_Triangel-1.caf");
    soundObjects[0]->setVariationJump(0);
    soundObjects[0]->setup();
    
    soundObjects[1]->setSound("Goodie_Shaker-1.caf");
    soundObjects[1]->setVariationJump(0);
    soundObjects[1]->setup();
    
    soundObjects[2]->setSound("Goodie_Cabasa-1.caf");
    soundObjects[2]->setVariationJump(0);
    soundObjects[2]->setup();
    
 
    soundObjects[3]->setSound("Goodie_Percussion_2-1.caf");
    soundObjects[3]->setVariationJump(0);
    soundObjects[3]->setSleepTime(500);
    soundObjects[3]->setup();

    soundObjects[4]->setSound("Goodie_Triangel-1.caf");
    soundObjects[4]->setVariationJump(0);
    soundObjects[4]->setSleepTime(500);
    soundObjects[4]->setup();
    
    
    soundscreenheight++;
     soundXpos=0;
    
    c.clear();
    c=generateColorSwatchesFromColor(soundObjects.size(),1,1,1,ofColor(0,255,0));
    
    for(int i=0; i<soundObjects.size(); i++){
        soundObjects[i]->setColor(c[i]);

        if(soundXpos>ofGetScreenWidth()-soundw){
            soundscreenheight++;
            soundXpos=0;
        }
        soundObjects[i]->setScreenpos(ofVec2f(soundXpos,soundscreenheight*soundh));
        soundXpos+=soundw;
    }
    
    c.clear();
    c=generateColorSwatchesFromColor(8,10,10,10,ofColor(200,0,0));
    
    goodies[0]->setSound("Goodie_Piano_1-1.caf");
    goodies[0]->setVariationJump(0);
    goodies[0]->setColor(c[0]);
    goodies[0]->setup();
    
    goodies[1]->setSound("Goodie_Piano_2-1.caf");
    goodies[1]->setVariationJump(0);
    goodies[1]->setColor(c[1]);
    goodies[1]->setup();
    
    goodies[2]->setSound("Goodie_Piano_3-1.caf");
    goodies[2]->setVariationJump(0);
    goodies[2]->setColor(c[2]);
    goodies[2]->setup();
    
    goodies[3]->setSound("Goodie_Piano_4-1.caf");
    goodies[3]->setVariationJump(0);
    goodies[3]->setColor(c[3]);
    goodies[3]->setup();
    
    goodies[4]->setSound("Goodie_Piano_5-1.caf");
    goodies[4]->setVariationJump(0);
    goodies[4]->setColor(c[4]);
    goodies[4]->setup();
    
    goodies[5]->setSound("Goodie_Piano_6-1.caf");
    goodies[5]->setVariationJump(0);
    goodies[5]->setColor(c[5]);
    goodies[5]->setup();

    c.clear();
    c=generateColorSwatchesFromColor(5,20,100,100,ofColor(0,0,255));

    goodies[6]->setSound("Goodie_Contrabass_1-1.caf");
    goodies[6]->setVariationJump(0);
    goodies[6]->setColor(c[0]);

    goodies[6]->setup();
    
    goodies[7]->setSound("Goodie_Contrabass_2-1.caf");
    goodies[7]->setVariationJump(0);
    goodies[7]->setColor(c[1]);

    goodies[7]->setup();
    
    c.clear();
    c=generateColorSwatchesFromColor(5,20,100,100,ofColor(127,109,79));

    goodies[8]->setSound("Goodie_Percussion_1-1.caf");
    goodies[8]->setVariationJump(0);
    goodies[8]->setColor(c[0]);
    goodies[8]->setup();
    
    goodies[9]->setSound("Goodie_Percussion_2-1.caf");
    goodies[9]->setVariationJump(0);
    goodies[9]->setColor(c[1]);

    goodies[9]->setup();
    
    goodies[10]->setSound("Goodie_Bongo-16bit.caf");
    goodies[10]->setVariationJump(0);
    goodies[10]->setColor(c[2]);
    goodies[10]->setup();

    c.clear();
    c=generateColorSwatchesFromColor(5,5,5,5,ofColor(204,96,69));

    goodies[11]->setSound("Goodie_Bandoneon_1-1.caf");
    goodies[11]->setVariationJump(0);
    goodies[11]->setColor(c[0]);
    goodies[11]->setup();

    goodies[12]->setSound("Goodie_Bandoneon_2-1.caf");
    goodies[12]->setVariationJump(0);
    goodies[12]->setColor(c[1]);
    goodies[12]->setup();
    
    goodies[13]->setSound("Goodie_Bandoneon_3-1.caf");
    goodies[13]->setVariationJump(0);
    goodies[13]->setColor(c[2]);
    goodies[13]->setup();
    
    goodies[14]->setSound("Goodie_Bandoneon_4-1.caf");
    goodies[14]->setVariationJump(0);
    goodies[14]->setColor(c[3]);

    goodies[14]->setup();
    
  
    
    soundscreenheight++;
    soundXpos=0;


    for(int i=0; i<goodies.size(); i++){
       // goodies[i]->setColor(ofColor(ofRandom(255),ofRandom(255),ofRandom(255)));
       if(i==6 ||i==10 || i==11 ){
           soundscreenheight++;
          soundXpos=0;
       }
        if(soundXpos>ofGetScreenWidth()-soundw){
            soundscreenheight++;
            soundXpos=0;
        }
        goodies[i]->setScreenpos(ofVec2f(soundXpos,soundscreenheight*soundh));
        soundXpos+=soundw;

    }
    
    soundscreenheight++;
    soundXpos=0;
    c=generateColorSwatchesFromColor(9,5,100,100,ofColor(255,241,24));

    vocals[0]->setSound("Goodie_Vocal_1-1.caf");
     vocals[0]->setColor(c[0]);
    vocals[0]->setup();
    vocals[1]->setSound("Goodie_Vocal_2-1.caf");
    vocals[1]->setColor(c[1]);
    vocals[1]->setup();
    vocals[2]->setSound("Goodie_Vocal_3-1.caf");
    vocals[2]->setColor(c[2]);
    vocals[2]->setup();
    vocals[3]->setSound("Goodie_Vocal_4-1.caf");
    vocals[3]->setColor(c[3]);
    vocals[3]->setup();
    vocals[4]->setSound("Goodie_Vocal_5-1.caf");
    vocals[4]->setColor(c[4]);
    vocals[4]->setup();
    vocals[5]->setSound("Goodie_Vocal_6-1.caf");
    vocals[5]->setColor(c[5]);
    vocals[5]->setup();
    vocals[6]->setSound("Goodie_Vocal_7-1.caf");
    vocals[6]->setColor(c[6]);
    vocals[6]->setup();
    vocals[7]->setSound("Goodie_Vocal_8-1.caf");
    vocals[7]->setColor(c[7]);
    vocals[7]->setup();
    vocals[8]->setSound("Goodie_Vocal_9-1.caf");
    vocals[8]->setColor(c[8]);
    vocals[8]->setup();
    
    
    soundscreenheight++;
    soundXpos=0;
    for(int i=0; i<vocals.size(); i++){
        
        if(soundXpos>ofGetScreenWidth()-soundw){
            soundscreenheight++;
            soundXpos=0;
        }
        vocals[i]->setScreenpos(ofVec2f(soundXpos,soundscreenheight*soundh));
        soundXpos+=soundw;

    }
    
    
    myActualBaseBeat= baseBeats[0];
    myNextBaseBeat=baseBeats[0];
    myActualBaseBeatIndex=0;
    myNextBaseBeatIndex=0;
    
      baseCycleDuration=myActualBaseBeat->getMySoundLength()-20;
    cout<<"base---"<<baseCycleDuration<<endl;

    
    bCycleTimeReached=false;
    bLoopBaseCycle=true; //not used?
    baseCycleIsRunning=false;
    
    tickcounter=0;

    if(!bRegisteredEvents) {
        ofRegisterMouseEvents(this); // this will enable our circle class to listen to the mouse events.
        ofRegisterTouchEvents(this); // this will enable our circle class to listen to the mouse events.
        bRegisteredEvents = true;
    }
    
    
    coreMotion.setupMagnetometer();
    coreMotion.setupGyroscope();
    coreMotion.setupAccelerometer();
    ofxAccelerometer.setForceSmoothing(.9);
    coreMotion.setupAttitude(CMAttitudeReferenceFrameXMagneticNorthZVertical);
   //coreMotion.setupAttitude(CMAttitudeReferenceFrameXArbitraryZVertical);

    coreLocation = new ofxiPhoneCoreLocation();
    hasCompass = coreLocation->startHeading();
    cout<<"has compass "<<hasCompass<<endl;
    heading = 0.0;

    
    compassImg.loadImage("zeiger.png");
    compassImg.setAnchorPoint(compassImg.width/2, 159);
    compassImg.update();
    
    
    moveenergy=0;
    moveenergy_slowdrag=0;
    rollCounter=0;

    initQuat = coreMotion.getQuaternion();
    //phonebody.setPosition(ofGetWidth()/2, ofGetHeight()/2, 0);
   // phonebody.rotate(initQuat);
    
    
    
    for(int i=0;i<50;i++){
        rollbuffer.push_back(0);
    }

    for(int i=0;i<50;i++){
        zbuffer.push_back(0);
    }

    
    for(int i=0;i<50;i++){
        accelerationbuffer.push_back(ofVec3f(0,0,0));
    }

    for(int i=0;i<50;i++){
        userAccelerationBuffer.push_back(ofVec3f(0,0,0));
    }
    
    for(int i=0;i<100;i++){
        headingbuffer.push_back(0);
    }
    
    
    
    ofTrueTypeFont::setGlobalDpi(72);

	font.loadFont("frabk.ttf", 70);
	font.setLineHeight(70);
	///font.setLetterSpacing(1.037);
    
    endBaseCycleTime=0;
    
    resetAllTickTargets();
    
}



//--------------------------------------------------------------
void Soundcontroller::update(){
    
    
    myGoodiesActualSleepDelay=ofMap(tickcounter,goodiesTickInit,goodiesTickTarget,myGoodieSleepDelay,300,true);
    myPianoActualSleepDelay=ofMap(tickcounter,pianoTickInit,pianoTickTarget,myPianoSleepDelay,300,true);
    myBandoneonActualSleepDelay=ofMap(tickcounter,bandoneonTickInit,bandoneonTickTarget,myBandonenonSleepDelay,300,true);
    myContrabassActualSleepDelay=ofMap(tickcounter,contrabassTickInit,contrabassTickTarget,myContrabassSleepDelay,300,true);
    
    cout<<"Piano "<<myPianoActualSleepDelay<< " Bandoneon " << myBandoneonActualSleepDelay<<" Contrabass "<<myContrabassActualSleepDelay<<endl;
    
       cout<<"Piano"<<bIsSleepingPiano<< " Bandoneon" << bIsSleepingBandoneon<<" Contrabass"<<bIsSleepingContrabass<<endl;
    
    
    
    float goodieTimer = ofGetElapsedTimeMillis() - goodieSleepStartTime;
    if(goodieTimer>myGoodiesActualSleepDelay){
        bIsSleepingGoodie=false;
    }

    
    float goodiePianoTimer = ofGetElapsedTimeMillis() - pianoSleepStartTime;
    if(goodiePianoTimer>myPianoActualSleepDelay){
        bIsSleepingPiano=false;
    }
   
    
    float goodieBandoneonTimer = ofGetElapsedTimeMillis() - bandoneonSleepStartTime;
    if(goodieBandoneonTimer>myBandoneonActualSleepDelay){
        bIsSleepingBandoneon=false;
    }
    
    
    float goodieContrabassTimer = ofGetElapsedTimeMillis() - contrabassSleepStartTime;
    if(goodieContrabassTimer>myContrabassActualSleepDelay){
        bIsSleepingContrabass=false;
    }
    
    
    float doublerTimer = ofGetElapsedTimeMillis() - doublerSleepStartTime;
    if(doublerTimer>mydoublerSleepDelay){
        bIsSleepingDouble=false;
    }
    
    
    float vocalTimer = ofGetElapsedTimeMillis() - vocalSleepStartTime;
    if(vocalTimer>myVocalSleepDelay){
        bIsSleepingVocal=false;
    }
    
    
    
    float soundObjectTimer = ofGetElapsedTimeMillis() - soundObjectSleepStartTime;
    if(soundObjectTimer>mySoundobjectsSleepDelay){
        bIsSleepingSoundObjects=false;
    }
    
    
    float shortSleepTimer = ofGetElapsedTimeMillis() - shortSleepStartTime;
    if(shortSleepTimer>shortSleepDelay){
        isShortSleep=false;
    }
    
    
    
    
    coreMotion.update();
    heading = ofLerpDegrees(heading, -coreLocation->getTrueHeading(), 0.6);
    cout<<heading<<endl;
   // cout<<heading<<endl;
   // cout<<ofxAccelerometer.getForce().y<<endl;
    
    
 //   CMQuaternion quat = self.motionManager.deviceMotion.attitude.quaternion;
  //  double yaw = asin(2*(quat.x*quat.z - quat.w*quat.y));
    
    
    
  //  roll  = Mathf.Atan2(2*y*w - 2*x*z, 1 - 2*y*y - 2*z*z);
  //  pitch = Mathf.Atan2(2*x*w - 2*y*z, 1 - 2*x*x - 2*z*z);
   // yaw   =  Mathf.Asin(2*x*y + 2*z*w);
    
    //ofQuaternion quat;
    ofQuaternion quat = coreMotion.getQuaternion();

    ofVec3f axis;//(0,0,1.0f);
    float angle;
    quat.getRotate(angle, axis);
    //quatpitch = atan2(2*quat.x()*quat.w() - 2*quat.y()*quat.z(), 1 - 2*quat.x()*quat.x() - 2*quat.z()*quat.z());
   
    float roll = atan2(2*quat.x()*quat.y() + 2*quat.w()*quat.z(), quat.w()*quat.w() + quat.x()*quat.x() - quat.y()*quat.y() - quat.z()*quat.z());
    
    
    quaternionRoll=roll;

    
    float yaw = atan2(2*quat.y()*quat.z() + 2*quat.x()*quat.w(), quat.w()*quat.w() - quat.x()*quat.x() - quat.y()*quat.y() + quat.z()*quat.z());

    
    quatpitch=  yaw;//asin(-2.0*(quat.x()*quat.z() - quat.w()*quat.y()));
    
    
    /*
    
    
    heading_angle=heading-heading_ref;
    heading_angle_goround=heading_angle;
    if(heading_angle_goround<0)heading_angle_goround=360-heading_angle_goround;
    heading_angle_modulo=(int)heading_angle % 360;
    */
    
    heading_angle=quaternionRoll-quaternionRoll_ref;
    heading_angle=ofMap(heading_angle, -PI, PI, 0, 360);
    
    cout<<"heading_angle"<<heading_angle<<endl;
    
    
    heading_angle_goround=heading_angle;
    
    if(heading_angle_goround<-PI)heading_angle_goround=PI-heading_angle_goround;
    heading_angle_modulo=(int)heading_angle % 360;
    
    
    
    
    
    
    delatheading=heading_angle_modulo-pheading_angle_modulo;
    
    heading_angle_goround=(int)heading_angle_goround % 360;
    
    ofVec3f acc = coreMotion.getUserAcceleration();//getAccelerometerData();
     uacc.set( coreMotion.getUserAcceleration());
    if(uacc.length()<0.05){
        uacc.set(0,0,0);
    }
    
    
     filteredUacc=uacc;
    //GET SMOOTH Z
    for(int i=accelerationbuffer.size()-1;i>0;i--){
        accelerationbuffer.at(i).set(accelerationbuffer.at(i-1).x,accelerationbuffer.at(i-1).y,accelerationbuffer.at(i-1).z);
    }
    
   
    if(ABS(accelerationbuffer.at(20).length()- uacc.length())>0.01){
        filteredUacc = accelerationbuffer.at(1).getInterpolated(uacc, 0.1);
    }else{
       // filteredUacc.set(0,0,0);
    }
    
    accelerationbuffer.at(0).set(filteredUacc);
    
    
    for(int i=headingbuffer.size()-1;i>0;i--){
        headingbuffer.at(i)=headingbuffer.at(i-1);
    }
    
    headingbuffer.at(0)=heading_angle;
    
    
    
    
    smoothedAcceleration=ofVec3f(0,0,0);
    ofVec3f temp_acc=ofVec3f(0,0,0);
    float sum=40;
    
    for(int i=0;i<sum;i++){
        temp_acc=accelerationbuffer.at(i);
       // temp_acc*=sum-i;
     //   sum+=sum-i;
        smoothedAcceleration+=temp_acc;
    }
    
    smoothedAcceleration/=sum;

    
    
    
    xx=acc.x;
     yy=acc.y;
     zz=acc.z;
    
     acceleration=ofVec3f(xx,yy,zz);
     accelerationLength=acceleration.length();
    
     dUacc=ofVec3f(pUacc);
    dUacc-=uacc;
  //if(dUacc.length()>maxAccDif) maxAccDif=dUacc.length();
    
    
    ofVec3f diffAcc=dUacc-=pDUacc;
    if(diffAcc.length()>maxAccDif) maxAccDif=diffAcc.length();

    
    if(accelerationLength>maxAccelerationlength){
        maxAccelerationlength=accelerationLength;
    }
    
    
    yaw=ofMap(coreMotion.getYaw(),-PI,PI,0,1,true);
    pitch=ofMap(coreMotion.getPitch(),-PI,PI,0,1,true);

    rawPitch=coreMotion.getPitch();
    rawYaw  = coreMotion.getYaw();
    rawRoll=coreMotion.getRoll();
    
    
    
    if(uacc.length()>2){
        //  int index =floor(ofRandom(vocals.size()));
        //vocals[index]->instantPlay();
    }
    

    
    
   int f=ofGetFrameNum()-framebefore;
   /*  if(f>70){
        framebefore=ofGetFrameNum();
        f=0;
        line.clear();
        line2.clear();
        match1=false;
        match2=false;
    
    }*/
    if(ABS(filteredUacc.length())<0.8){
        framebefore=ofGetFrameNum();
        f=0;
        line.clear();
        line2.clear();
        match1=false;
        match2=false;
        lineframe=0;

    }
    
    if(line.size()>50){
        framebefore=ofGetFrameNum();
        f=0;
        line.clear();
        line2.clear();
        match1=false;
        match2=false;

        lineframe=0;
     
    }

    

    
    
  
    
    
    
    
    if(ABS(filteredUacc.length())>8){
        lineframe++;
        if(f>ofGetWidth()/2){
            framebefore=ofGetFrameNum();
            f=0;
            lineframe=0;
            line.clear();
            line2.clear();
            match1=false;
            match2=false;

        }
       // line.push_back(ofVec2f(filteredUacc.x*100,filteredUacc.y*100));

       line.push_back(ofVec2f(lineframe,filteredUacc.x*100+ofGetWidth()/2));
        line2.push_back(ofVec2f(lineframe,filteredUacc.y*100+ofGetWidth()/2));
        
        
       // line=resample(500,line);
       // line2=resample(500,line2);

      //  yline.push_back(ofVec2f(f,filteredUacc.y*100+ofGetWidth()/2));

    }
    
    
   
    
   // bool match1=false;
   // bool match2=false;
    ofxGesture* t = new ofxGesture();
    
    for(int i = 0; i < line.size(); ++i) {
        t->addPoint(line[i].x, line[i].y);
    }
    
    if(dollar.gestures.size()>0 && t->points.size()>20){
        double score = 0.0;
        ofxGesture* match = dollar.match(t, &score);
        if(match != NULL && score >0.8) {
           // ofBackground(0,0,255);
           // line.clear();
            match1=true;
            cout<<"------------match1--------SCORE------------------"<<endl;

        }
        
    }
    
    t = new ofxGesture();
    
    for(int i = 0; i < line2.size(); ++i) {
        // cout<<"add "<<i<<endl;
        t->addPoint(line2[i].x, line2[i].y);
    }
    
    if(dollar2.gestures.size()>0 && t->points.size()>20){
        double score = 0.0;
        ofxGesture* match = dollar2.match(t, &score);
        if(match != NULL && score >0.8) {
          //  ofBackground(0,255,0);
           // line2.clear();
            match2=true;
            cout<<"------------match2--------SCORE------------------"<<endl;


        }
        
    }

    if(match1&&match2){
        cout<<"---------------------SCORE------------------"<<endl;
        //ofBackground(255, 0, 0);
        match1=false;
        match2=false;
        
        framebefore=ofGetFrameNum();
        f=0;
        lineframe=0;
        line.clear();
        line2.clear();
     

    }
    
    
   
    

    moveenergy+=ABS(coreMotion.getUserAcceleration().length());
    moveenergy*=HARDENERGYDRAG;
    
    if(moveenergy>maxMoveEnergy)maxMoveEnergy=moveenergy;
    
    moveenergy_slowdrag+=ABS(coreMotion.getUserAcceleration().length());
    moveenergy_slowdrag*=ENERGYDRAG;
    
    if(moveenergy_slowdrag>maxMoveEnergySlowdrag)maxMoveEnergySlowdrag=moveenergy_slowdrag;
    
    float speed=ofMap(moveenergy,25,30,0,1,true);
    if(moveenergy>1 && bCycleTimeReached)startBaseCycle();
    if(moveenergy<1){
       stopBaseCycle();
        setIsBreak(false);
    }
    
    if(moveenergy_slowdrag>5){
    }
    
 
    
    
    
    //GET SMOOTH Z
    for(int i=zbuffer.size()-1;i>0;i--){
        zbuffer.at(i)=zbuffer.at(i-1);
    }
    zbuffer.at(0)=filteredUacc.z;
    
    smoothed_z=0;
    float temp_z;
    float sumpos_z=20;
    
    for(int i=0;i<20;i++){
        temp_z=zbuffer.at(i);
        // temp*=10-i;
        // sumpos+=10-i;
        smoothed_z+=temp_z;
    }
    
    smoothed_z/=sumpos_z;
    deltaSmoothZ=smoothed_z-psmoothed_z;
    
    zCounter+= ABS(deltaSmoothZ);
    zCounter*=ENERGYDRAG;
    zCounterColor.set(255);
    
    if(zCounter>0.6 && ofGetFrameNum()>200){
        zCounterColor.set(255,0,0);
    }

    if(zCounter>2 && ofGetFrameNum()>200){
        
    
    
    }else{
    }
    
    
    roll=coreMotion.getRoll();
    float absRoll=ABS(coreMotion.getRoll());
    for(int i=rollbuffer.size()-1;i>0;i--){
        rollbuffer.at(i)=rollbuffer.at(i-1);
    }
    rollbuffer.at(0)=absRoll;
    
    smoothed_roll=0;
    float temp;
    float sumpos=20;

    for(int i=0;i<10;i++){
        temp=rollbuffer.at(i);
       // temp*=sumpos-i;
       // sumpos+=sumpos-i;
        smoothed_roll+=temp;
    }

    smoothed_roll/=10;
    
    if(smoothed_roll>maxSmooth_roll)maxSmooth_roll=smoothed_roll;
    
    
    if(ABS(roll-smoothed_roll)>maxRollDif){maxRollDif=ABS(roll-smoothed_roll);}
    
    deltaSmoothRoll=smoothed_roll-psmoothed_roll;
    mappedRoll=ofMap(smoothed_roll,-PI,PI,0,1,true);

    
    if(ABS(deltaSmoothRoll)>0.00015){
       if(smoothed_roll>psmoothed_roll){
            roll_clockwise=true;
        }else{
            roll_clockwise=false;
        }
    }
    
    
    rollCounter+=ABS(deltaSmoothRoll);
    rollCounter*=ENERGYDRAG;
    rollCounterColor.set(255,255,255);

    
    float s=20;
    smoothedX=0;
    float tmp;
    for(int i=0;i<s;i++){
        tmp=ABS(userAccelerationBuffer.at(i).x);
        // temp*=sumpos-i;
        // sumpos+=sumpos-i;
        smoothedX+=tmp;
    }
    smoothedX/=s;

    
    float deltaX=ABS(filteredUacc.x)-ABS(pfilterdUacc.x);
    xCounter+=ABS(deltaX);
    xCounter*=ENERGYDRAG;
 
    
    
    float deltaY=ABS(filteredUacc.y)-ABS(pfilterdUacc.y);
    yCounter+=ABS(deltaY);
    yCounter*=ENERGYDRAG;

    
    float deltaZ=ABS(filteredUacc.z)-ABS(pfilterdUacc.z);
    z2Counter+=ABS(deltaZ);
    z2Counter*=ENERGYDRAG;
    
    
/*
 
 
 
 
 
  int  angles=3;
    
    if(tickcounter>PARALLELTICKTRIGGER)angles=6;

    for (int i=0; i<angles; i++) {
        if(ABS(heading_angle_modulo)>(i)*(360/angles) && ABS(heading_angle_modulo) < (1+i)*(360/angles)){
            if(i>2)i=i-3;
            setChordOffset(i);
            if(moveenergy_slowdrag>30 && tickcounter>PARALLELTICKTRIGGER){
               //  if(rawPitch>PITCHTRIGGER){
                setIsParallel(true);
            }else{
                setIsParallel(false);
                
            }
        }
    }*/
    
    
    
 //Chord Offset
    
    
    if(heading_angle_goround > 0 && heading_angle_goround < 120){
        setChordOffset(0);
        // if(moveenergy_slowdrag>30 && tickcounter>PARALLELTICKTRIGGER){
        if(tickcounter >PARALLELTICKTRIGGER && quatpitch>PITCHTRIGGER){
           // setIsParallel(true);
        }else{
            //setIsParallel(false);
            
        }
    }
    
    
     if(heading_angle_goround>120&& heading_angle_goround < 240){
         setChordOffset(1);
        // if(moveenergy_slowdrag>30 && tickcounter>PARALLELTICKTRIGGER){
         if(tickcounter >PARALLELTICKTRIGGER && quatpitch>PITCHTRIGGER){
         //setIsParallel(true);
         }else{
          //   setIsParallel(false);
         }
    }
    
    
     if(heading_angle_goround>240 && heading_angle_goround < 360){
         setChordOffset(2);
        // if(moveenergy_slowdrag>30 && tickcounter>PARALLELTICKTRIGGER){
         if(tickcounter > PARALLELTICKTRIGGER && quatpitch>PITCHTRIGGER){
            // setIsParallel(true);
         }else{
           //  setIsParallel(false);
         }

    }
 
    /*
    float dHeading=heading_angle-headingbuffer.at(3);
    if(tickcounter>5){
    if(dHeading>50)addPianoGoodieToPlayAt();
    if(dHeading<-50)addContrabassGoodieToPlayAt();
    }*/
    
    
    
    
    
       float diffHeading=heading_angle-headingbuffer.at(80);
    
   //->hier Energieabhängig machen
    if(ABS(diffHeading)>290){
        setIsParallel(!getIsParallel());
        resetHeadingBuffer();
    }
    
    
       if(soundIdHistory.size()>3){
       //cout<<soundIdHistory.back()<<" "<<soundIdHistory[soundIdHistory.size()-2]<<" "<<soundIdHistory[soundIdHistory.size()-3]<<endl;
        if(soundIdHistory.back()==2 &&soundIdHistory[soundIdHistory.size()-2]==1 && soundIdHistory[soundIdHistory.size()-3]==0){
            setIsBreak(true);
        }
    }
    
    
   
    
    if(moveenergy_slowdrag< 45){
        setVariationOffset(0);
    }
    

    if(moveenergy_slowdrag>ENERGLOWYTRIGGER && tickcounter>4){
        setVariationOffset(1);
    }
    
    
    if(moveenergy_slowdrag> ENERGYHIGHTRIGGER && tickcounter>8){
        setVariationOffset(2);
    }
    
 
    
    
    // -------------------- Ticker Logic ----------------------------
    float dHeading=heading_angle-headingbuffer.at(3);
   
    
   // if(tickcounter<9)addRomanticDoublerToQueue();
    
    
    
    
    if(tickcounter%3==1){
        if(tickcounter!=ptickcounter){
        resetAllTickTargets(50);
        float r=ofRandom(1);
        cout<<"-----------------------r-------------------- "<<r<<endl;
        if(r<0.3)startPianoCrescendoTicker();
        if(r>0.3&&r<0.6)startBandoneonCrescendoTicker();
        if(r>0.6)startContrabassCrescendoTicker();
        }
    }
    
    
    
    if(tickcounter==1){
        //resetAllTickTargets(50);
     //   startPianoCrescendoTicker();
        
       /* myPianoSleepDelay=3000;
        pianoTickTarget=10;
        pianoTickInit=tickcounter;
        bIsSleepingPiano=false;*/
    
    }
    
    
    
  
    
    
    //Add Breaks
    if(tickcounter==3){
        setIsBreak(true);
      //  myPianoSleepDelay=3000;
       // pianoTickTarget=10;
       // pianoTickInit=tickcounter;
    }
    
    if(tickcounter==4){
        //resetAllTickTargets(50);
        //startBandoneonCrescendoTicker();
    }
    
    
    if(tickcounter==7){
       // resetAllTickTargets();
       // myBandonenonSleepDelay=3000;
        //bandoneonTickTarget=10;
        //bandoneonTickInit=tickcounter;
    }
 
    
    
    
    if(tickcounter % 12 == 0 & tickcounter >1){
        setIsBreak(true);
    }
    
  
    if(tickcounter==4 ){
        setVariationOffset(2);
    }
    
   
    if(tickcounter>9 && tickcounter<12){
        setVariationOffset(0);
    }
    
    
    if(tickcounter>9 && tickcounter<20){
        if( dHeading>30){
            addRomanticDoublerToQueue();
            setVariationOffset(0);
        }
    }
    
    
    
    
    
    if(tickcounter>5){
        if(ABS(dHeading)>50)addContrabassGoodieToPlayAt();
    }
    
    
    if(tickcounter>20 && tickcounter<24){
       setVariationOffset(0);
    }

    
    if(tickcounter>5&&tickcounter<7){
    }
 
    if(tickcounter>6&&tickcounter<8){
       // setNextBaseBeat(parallels[1]);
        //setIsParallel(true);
    }
    
    
    
    if(tickcounter>6&&tickcounter<8){
        // setNextBaseBeat(parallels[1]);
        //setIsParallel(true);
    }
    
   
    
    //Diff Acc
    
    //Diff Acc
    if(ABS(diffAcc.length())>1.5){
        
        //addRandomSoundObjectInstantPlay(false,1000);
        
       // addPianoGoodieToPlayAt(true,100);
        addPianoGoodieToPlayAt(true,100);
        addBandoneonGoodieToPlayAt(true,100);
        addContrabassGoodieToPlayAt(true,100);

       //addRandomGoodieInstantPlay(false,5000);

    };
    
    
    if(ABS(diffAcc.length())>3.9 && tickcounter>9){
        if(tickcounter>10){
           addRandomVocalsInstantPlay(true,1000);
        }  if(tickcounter>15){
        
        }
        
    }
    
    
    
    float dot=filteredUacc.dot(userAccelerationBuffer[2]);
    if(ABS(dot)>2 && ABS(uacc.length())>0.01){
       
       // addPianoGoodieToPlayAt();
      //  if(ofRandom(1)>0.5)addRandomGoodieToQueue(false);

        
        if(ABS(uacc.x)>ABS(uacc.y)&&ABS(uacc.x)>ABS(uacc.z)){
            cout << "------------------------ x Dot"<<uacc.x<<endl;
          
            
           // addBandoneonGoodieToQueue();
            
        }
     
        if(ABS(uacc.y)>ABS(uacc.x)&&ABS(uacc.y)>ABS(uacc.z)){
            cout << "------------------------- y Dot "<<uacc.y<<endl;
           // addContrabassGoodieToQueue();
            
        }
        
        if(ABS(uacc.z)>ABS(uacc.x)&&ABS(uacc.z)>ABS(uacc.y)){
            cout << "-------------------------- z Dot "<<uacc.z<<endl;
          //  addPercussionGoodieToQueue();
            
        }

        

       // setIsBreak(true);
    }
    
  //  cout<<"roll - smoothed roll "<<roll-smoothed_roll<<endl;
    if(ABS(roll-smoothed_roll)>4){
        
    }
    
    
    
    
    //User Acceleration
    if(ABS(filteredUacc.length())>2 && ofGetFrameNum()>200){
        // addRandomSoundObjectInstantPlay();
        //addPercussionGoodieToQueue(false);
        //addRandomSoundObjectInstantPlay();
        if(tickcounter % 9 <3){
           // addBandoneonGoodieToQueue();
        }
        if(tickcounter % 9 >2 && tickcounter % 9< 6){
           // addContrabassGoodieToQueue();
        }
        
        if(tickcounter % 9 >5 && tickcounter % 9< 9){
           // addPianoGoodieToQueue();
        }
        
    }
    
    
    if(ABS(filteredUacc.length())>3 && ofGetFrameNum()>200){
        //addRandomSoundObjectInstantPlay(false);
    }
    
    
    
    if(xCounter > 3 ){
       // addPianoGoodieToQueue(true);
        resetXCounter();
    }
    
    if(yCounter > 3){
       // addContrabassGoodieToQueue(true);
        resetYCounter();
    }
    
    if(z2Counter > 5 && tickcounter>10){
     //  addBandoneonGoodieToQueue(true);
        resetZCounter();
    }
    
    
    if(rollCounter>2.5 && tickcounter >5){
       // addPianoGoodieToPlayAt();
    }
    if(rollCounter<2 && ofGetFrameNum()>200){
     
    
    }
    if(rollCounter>3 && ofGetFrameNum()>200){
      //  addPianoGoodieToQueue();
        }
    
    
    
    
    // update the timer this frame
    float timer = ofGetElapsedTimeMillis() - startBaseCycleTime;
    
    if(timer >= endBaseCycleTime/2 && !bCycleTimeReachedHalf && baseCycleIsRunning) {
   halfBaseCycle();
    }
    
    
    
    
    if(timer >= endBaseCycleTime && !bCycleTimeReached) {
        
        cout<<"timer end"<<endl;
        bCycleTimeReached = true;
        baseCycleIsRunning=false;
        ofMessage msg("Timer End");
        ofSendMessage(msg);
        ofNotifyEvent(cycleEnded, tickcounter, this);
       
        if(bLoopBaseCycle)startBaseCycle();
    }
    // get the percantage of the timer
    if(timer>0 && endBaseCycleTime>0)actualBaseCyclePos = ofMap(timer, 0.0, endBaseCycleTime, 0.0, 1.0, true);
    for (int i=0;i<baseBeats.size();i++){
    baseBeats[i]->update();
    }
    
    
    for (int i=0;i<doublers.size();i++){
        doublers[i]->update();
    }
    
    for (int i=0;i<soundObjects.size();i++){
        soundObjects[i]->update();
    }
    for (int i=0;i<goodies.size();i++){
       goodies[i]->update();
    }
 
    for (int i=0;i<vocals.size();i++){
        vocals [i]->update();
    }
    
//    checkMuteSounds();
    
    
    
   /*
    -> im Draw
    rollbefore=roll;
    
    px=xx;
    py=yy;
    pz=zz;
    
    pAcceleration.set(xx,yy,zz);
    pAccelerationLength=pAcceleration.length();
    */
    
    
    /*if(bIsParallel){
        ofBackground(255, 0, 0);
        
    }
    else{
        ofBackground(255);
    }*/
  //  ofColor bk=variationcol[chordOffset];
  // bk.setSaturation(50);
  //  ofBackground(bk);

    
    
    if(isTouched){
        int f=ofGetFrameNum()-recframebefore2;
      //  recline.push_back(ofVec2f(f,filteredUacc.x*100+ofGetWidth()/2));
       // recline2.push_back(ofVec2f(f,filteredUacc.y*100+ofGetWidth()/2));
    }
    
    
}


float Soundcontroller::fadeTo(float _fadeTarget, float _actualfade, float _fadespeed){
    float target =_fadeTarget;
    float actualfade=_actualfade;
    float fadespeed=_fadespeed;
    float fadeDistance = target-actualfade;
        if(abs(fadeDistance)<0.01){
            actualfade=target;
            return actualfade;
        }
        float fadefact=fadeDistance*fadespeed;
        actualfade+=fadefact;
    return actualfade;
}


//--------------------------------------------------------------
void Soundcontroller::draw(){
    
    
    if(isUpright){
    int myrectwidth=360;//ofGetWidth();
    int rectheight=ofGetHeight();
    float movetarget;
   
        if(getIsParallel()){
        movetarget=ofGetHeight()+rectheight/2;
    }else{
        movetarget=rectheight/2;
    }
    
   actualFade=fadeTo(movetarget, actualFade, 0.2);
    float yOffset=actualFade;
    
        
        
        
        
        int mytestrectwidth=ofGetWidth();
        int mytestrectheight=rectheight=ofGetHeight();
        
        ofPushMatrix();
        ofTranslate(ofGetWidth()/2,yOffset);
        //ofTranslate(3*myrectwidth/360*heading_angle_modulo,-rectheight/2);
        
        int transangle=heading_angle_modulo;
       // if(transangle>360)transangle=0+transangle-360;
        //if(transangle<-360)transangle=0;
        ofTranslate(mytestrectwidth/120*transangle,0);

    //    cout<<transangle<<endl;
        
        
        ofVec2f pos=ofVec2f(0,0);
        ofVec2f pos2=ofVec2f(mytestrectwidth,0);
        
    

        
       // mytestrectwidth
       
        ofColor myCol=variationcol[0];

        for (int i=0;i<3;i++){
            myCol=(variationcol[i]);//,ofMap(moveenergy, 0, maxMoveEnergySlowdrag,100, 255,true));
            int b=myCol.getBrightness();
            int s=myCol.getSaturation();
        
           // myCol.setBrightness(ofMap(moveenergy_slowdrag, 0, 30,50, b,true));
           // myCol.setSaturation(ofMap(moveenergy_slowdrag, 0, 30, s-50, s,true));
            
            ofSetColor(myCol);
            ofRect(i*mytestrectwidth, 0, mytestrectwidth, mytestrectheight);
            
            myCol.setBrightness(b);
            ofSetColor(myCol);
            string typeStr;
            typeStr=ofToString(i,3);
            font.drawString(typeStr, i*myrectwidth+(myrectwidth/2), rectheight/3);
            
        }

        

        ofPopMatrix();

        
        
        
        /*
   
    ofPushMatrix();
    ofTranslate(ofGetWidth()/2,yOffset);
    ofTranslate(3*myrectwidth/360*heading_angle_modulo,-rectheight/2);
    
    ofPushMatrix();
   // ofColor myCol=variationcol[0];
    for (int i=0;i<3;i++){
        myCol=(variationcol[i]);//,ofMap(moveenergy, 0, maxMoveEnergySlowdrag,100, 255,true));
        int b=myCol.getBrightness();
        int s=myCol.getSaturation();

        myCol.setBrightness(ofMap(moveenergy_slowdrag, 0, 30,50, b,true));
        myCol.setSaturation(ofMap(moveenergy_slowdrag, 0, 30, s-50, s,true));
        
        ofSetColor(myCol);
        ofRect(i*myrectwidth, 0, myrectwidth, rectheight);
        
        myCol.setBrightness(b);
        ofSetColor(myCol);
        string typeStr;
        typeStr=ofToString(i,3);
        font.drawString(typeStr, i*myrectwidth+(myrectwidth/2), rectheight/3);

    }
    ofPopMatrix();
    
    ofPushMatrix();
    ofTranslate(-3*myrectwidth,0);
    for (int i=0;i<3;i++){
        myCol=(variationcol[i]);//,ofMap(moveenergy, 0, maxMoveEnergySlowdrag,100, 255,true));
        int b=myCol.getBrightness();
        int s=myCol.getSaturation();


        myCol.setBrightness(ofMap(moveenergy_slowdrag, 0, 30,50, b,true));
        myCol.setSaturation(ofMap(moveenergy_slowdrag, 0, 30, s-50, s,true));

        ofSetColor(myCol);
        ofRect(i*myrectwidth, 0, myrectwidth, rectheight);
        
        myCol.setBrightness(b);
        ofSetColor(myCol);
        string typeStr;
        typeStr=ofToString(i,3);
        font.drawString(typeStr, i*myrectwidth+(myrectwidth/2), rectheight/3);

        
    }
    ofPopMatrix();
    
    ofTranslate(3*myrectwidth,0);
    ofPushMatrix();
    for (int i=0;i<3;i++){
        myCol=(variationcol[i]);//,ofMap(moveenergy, 0, maxMoveEnergySlowdrag,100, 255,true));
        int b=myCol.getBrightness();
        int s=myCol.getSaturation();

        myCol.setBrightness(ofMap(moveenergy_slowdrag, 0, 30,50, b,true));
        myCol.setSaturation(ofMap(moveenergy_slowdrag, 0, 30, s-50, s,true));
        ofSetColor(myCol);
        ofRect(i*myrectwidth, 0, myrectwidth, rectheight);
        
        myCol.setBrightness(b);
        ofSetColor(myCol);
        string typeStr;
        typeStr=ofToString(i,3);
        font.drawString(typeStr, i*myrectwidth+(myrectwidth/2), rectheight/3);
        
    }
    ofPopMatrix();
   
        
        
    ofPopMatrix();
    
    
    
    ofPushMatrix();
    ofTranslate(ofGetWidth()/2,yOffset);
    ofTranslate(3*myrectwidth/360*heading_angle_modulo,-rectheight/2-rectheight);
   //     ofTranslate(myrectwidth/120*heading_angle_goround,-rectheight/2);

    ofPushMatrix();
    // ofTranslate(screenpos.x, screenpos.y, 0);
    for (int i=0;i<3;i++){
        myCol=(variationcol[i]);//,ofMap(moveenergy, 0, maxMoveEnergySlowdrag,100, 255,true));
        int b=myCol.getBrightness()+20;
        int s=myCol.getSaturation();

        myCol.setBrightness(ofMap(moveenergy_slowdrag, 0, 60,50, b,true));
        myCol.setSaturation(ofMap(moveenergy_slowdrag, 0, 60, s-50, s,true));
        myCol.setHue(myCol.getHue()-20);
        ofSetColor(myCol);
        ofRect(i*myrectwidth, 0, myrectwidth, rectheight);
        
        myCol.setBrightness(b);
        ofSetColor(myCol);
        string typeStr;
        typeStr=ofToString(i,3);
        typeStr+="B";

        font.drawString(typeStr, i*myrectwidth+(myrectwidth/2), rectheight/3);
    }
    ofPopMatrix();
    
    ofPushMatrix();
    ofTranslate(-3*myrectwidth,0);
    for (int i=0;i<3;i++){
        myCol=(variationcol[i]);//,ofMap(moveenergy, 0, maxMoveEnergySlowdrag,100, 255,true));
        int b=myCol.getBrightness()+20;
        int s=myCol.getSaturation();

        myCol.setBrightness(ofMap(moveenergy_slowdrag, 0, 60,50, b,true));
        myCol.setSaturation(ofMap(moveenergy_slowdrag, 0, 60, s-50, s,true));
        myCol.setHue(myCol.getHue()-20);

        ofSetColor(myCol);
        ofRect(i*myrectwidth, 0, myrectwidth, rectheight);
       
        myCol.setBrightness(b);
        ofSetColor(myCol);
        string typeStr;
        typeStr=ofToString(i,3);
        typeStr+="B";

        font.drawString(typeStr, i*myrectwidth+(myrectwidth/2), rectheight/3);
        
    }
    ofPopMatrix();
    ofTranslate(3*myrectwidth,0);
    ofPushMatrix();
    for (int i=0;i<3;i++){
        myCol=(variationcol[i]);//,ofMap(moveenergy, 0, maxMoveEnergySlowdrag,100, 255,true));
        int b=myCol.getBrightness()+20;
        int s=myCol.getSaturation();

        myCol.setBrightness(ofMap(moveenergy_slowdrag, 0, 60,50, b,true));
        myCol.setSaturation(ofMap(moveenergy_slowdrag, 0, 60, s-50,s,true));
        myCol.setHue(myCol.getHue()-20);

        myCol.setBrightness(b);
        ofSetColor(myCol);
        string typeStr;
        typeStr=ofToString(i,3);
        typeStr+="B";

        font.drawString(typeStr, i*myrectwidth+(myrectwidth/2), rectheight/3);
        
    }
    ofPopMatrix();
    ofPopMatrix();
     */

    }

    
    
    
    ofSetColor(255);
    string typeStr;
    typeStr=ofToString(xCounter,3);

    font.drawString(typeStr, 30, 70);
    ofDrawBitmapStringHighlight("xCounter",30, 70,ofColor(255,0,0));


    
    typeStr=ofToString(quatpitch,3);
    //font.drawString(typeStr, 30, 140);
    
    typeStr=ofToString(yCounter,3);
    font.drawString(typeStr, 30, 130);
    ofDrawBitmapStringHighlight("yCounter",30, 130,ofColor(255,0,0));

    
    typeStr=ofToString(z2Counter,3);
    font.drawString(typeStr, 30, 200);
    ofDrawBitmapStringHighlight("z2Counter",30, 200,ofColor(255,0,0));

    
    //maxAccDif
    
    
    typeStr=ofToString(getIsParallel(),3);
    font.drawString(typeStr, 30, 270);
    ofDrawBitmapStringHighlight("is Parallel",30, 270,ofColor(255,0,0));

    typeStr=ofToString(moveenergy_slowdrag,3);
    font.drawString(typeStr, 30, 340);
    ofDrawBitmapStringHighlight("moveenergy_slowdrag",30, 340,ofColor(255,0,0));
    
    float diffHeading=heading_angle-headingbuffer.at(80);
    typeStr=ofToString(diffHeading,3);
    font.drawString(typeStr, 30, 410);
    ofDrawBitmapStringHighlight("diffHeading",30, 410,ofColor(255,0,0));
    
   
    typeStr=ofToString(maxAccDif,3);
    font.drawString(typeStr, 30, 480);
    ofDrawBitmapStringHighlight("maxAccDif",30, 480,ofColor(255,0,0));
    

    
    
    
    
    float timer = ofGetElapsedTimeMillis() - startBaseCycleTime;
    typeStr="1";
    if(timer >= endBaseCycleTime/4)typeStr="2";
    if(timer >= endBaseCycleTime/2)typeStr="3";
    if(timer >= endBaseCycleTime/4*3)typeStr="4";

    
    ofSetColor(20);
    font.drawString(typeStr, ofGetWidth()/2, ofGetHeight()/2);
  
   ofDrawBitmapStringHighlight(ofToString(tickcounter,3), 10,50,ofColor(255,0,0));
    
    if(quatpitch>PITCHTRIGGER){
    //ofBackground(255, 0, 0);
    }else{
      //  ofBackground(0, 255, 0);

    }
    
   
   
    for (int i=0;i<baseBeats.size();i++){
        baseBeats[i]->draw();
    }
    
    
    for (int i=0;i<soundObjects.size();i++){
        soundObjects[i]->draw();
    }
    
    
    
    for (int i=0;i<goodies.size();i++){
        goodies[i]->draw();
    }
    
    
    for (int i=0;i<vocals.size();i++){
        vocals[i]->draw();
    }
    
    for (int i=0;i<doublers.size();i++){
        doublers[i]->draw();
    }
    
    
  /*  ofPushMatrix();
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
    ofRotate(180);
    ofColor seqcol;
    for(int i=0;i<moveenergy*10;i++){
        seqcol.set(0,255,255,ofMap(i, 0, moveenergy*10, 255, 0)*ofMap(moveenergy, 0, 20, 0, 1,true));
        seqcol.setHueAngle(20+ofMap(i, 0, moveenergy*10, 0, 20));
        ofSetColor(seqcol);
        float arc=actualBaseCyclePos-(i*0.003);
        ofPushMatrix();
        ofRotate(ofMap(arc,0,1,0,360));
        ofRect(0, 0, 2, ofMap(moveenergy, 0, 20, 40, 150,true));
        ofPopMatrix();
        
    }
   // ofSetColor(255,255,255,ofMap(moveenergy, 0, 20, 200, 255,true));
  //  ofPushMatrix();
    // ofRotate(ofMap(actualBaseCyclePos,0,1,0,360));
    // ofRect(0, 0, 2, ofMap(moveenergy, 0, 20, 40, 150,true));
   // ofPopMatrix();
    // ofRect(0, 0, 1, ofMap(moveenergy, 0, 20, 40, 150,true));
   // ofNoFill();
    //ofEllipse(0, 0, ofMap(moveenergy, 0, 20, 80, 300,true), ofMap(moveenergy, 0, 20, 80, 300,true));
   // ofFill();
    ofPopMatrix();
    
    */

/*
    
    ofSetColor(0,0,255);
    // glBegin(GL_LINE_STRIP);
    for(int i = 1; i < line.size(); ++i) {
        ofLine(line[i-1].x, line[i-1].y, line[i].x, line[i].y);
    }
    ofSetColor(255);
    for(int i = 1; i < line2.size(); ++i) {
       ofLine(line2[i-1].x, line2[i-1].y, line2[i].x, line2[i].y);
    }
    
    ofSetColor(255,0,0);
    for(int i = 1; i < recline.size(); ++i) {
        ofLine(recline[i-1].x, recline[i-1].y, recline[i].x, recline[i].y);
    }
    ofSetColor(0,255,0);
    for(int i = 1; i < recline2.size(); ++i) {
        ofLine(recline2[i-1].x, recline2[i-1].y, recline2[i].x, recline2[i].y);
    }
    */
    
    ptickcounter=tickcounter;
    
    rollbefore=roll;
    
    pRoll=roll;
    pYaw=yaw;
    pPitch=pitch;
    
    px=xx;
    py=yy;
    pz=zz;
    
    pAcceleration.set(xx,yy,zz);
    pAccelerationLength=pAcceleration.length();
    proll_clockwise=roll_clockwise;
    psmoothed_roll=smoothed_roll;
    psmoothed_z=smoothed_z;
    pheading=heading;
    pUacc.set(uacc.x,uacc.y,uacc.z);
    
    pfilterdUacc=filteredUacc;
    
    
    
    
    pheading_angle_modulo=heading_angle_modulo;
     pDeltaheading=delatheading;
    pHeadingSpeed= headingSpeed;
    
    
    
    for(int i=userAccelerationBuffer.size()-1;i>0;i--){
        userAccelerationBuffer.at(i).set(userAccelerationBuffer.at(i-1).x,userAccelerationBuffer.at(i-1).y,userAccelerationBuffer.at(i-1).z);
    }
    userAccelerationBuffer[0].set(uacc);
    
}




void Soundcontroller::setNoHalfCicle(bool _noHalfCiclye){

    bNoHalfCicle=_noHalfCiclye;



}



void Soundcontroller::createNewGesture(){
    ++num_created_gestures;
    gesture = new ofxGesture();
    gesture->setName("Gesture#" +ofToString(num_created_gestures));
    
    gesture2 = new ofxGesture();
    gesture2->setName("Gesture#" +ofToString(num_created_gestures));
}

//--------------------------------------------------------------
void Soundcontroller::startBaseCycle(){
    cout<<"startBaseCycle"<<endl;
    bCycleTimeReachedHalf=false;
  
    
    
    if(!baseCycleIsRunning){
    bCycleTimeReached=false;
        int c =tickcounter;
        
        if(myActualBaseBeat!=myNextBaseBeat){
            cout<<"------ Change base beat! ------------"<<baseCycleDuration<<" "<<nextBaseCycleDuration<<endl;
            setActualBaseBeat(myNextBaseBeat);
            baseCycleDuration=nextBaseCycleDuration;
        }
        
        myActualBaseBeat->addToQueue();
        startBaseCycleTime = ofGetElapsedTimeMillis();  // get the start time
        endBaseCycleTime = baseCycleDuration; // in milliseconds
        ofNotifyEvent(cycleStart, c, this);
        baseCycleIsRunning=true;
    }
    if(getIsBreak())muteAllSpecials();
    soundIdHistory.push_back(getChordOffset());
    tickcounter++;
}


//--------------------------------------------------------------
void Soundcontroller::halfBaseCycle(){
    bCycleTimeReachedHalf=true;
    if(getIsBreak() || bNoHalfCicle){
        setNoHalfCicle(false);
        return;
    }
    ofNotifyEvent(cicleHalf, tickcounter, this);
    if(getIsBreak())muteAllSpecials();

    soundIdHistory.push_back(getChordOffset());


}




//--------------------------------------------------------------
void Soundcontroller::stopBaseCycle(){

    if(baseCycleIsRunning){
        cout<<"stop"<<endl;

    bCycleTimeReached=true;
    bLoopBaseCycle=false;
        //endBaseCycleTime = baseCycleDuration;
    endBaseCycleTime =0;
    int c =0;
    ofNotifyEvent(cycleStop, c, this);
    baseCycleIsRunning=false;
    }
    soundIdHistory.clear();
    tickcounter=0;
    
}


//--------------------------------------------------------------
float Soundcontroller::getBaseCyclePos(){
    return actualBaseCyclePos;
}


void Soundcontroller::muteAllSpecials(){
    for (int i=0;i<soundObjects.size();i++){
        soundObjects[i]->pausePlay();
    }
    for (int i=0;i<goodies.size();i++){
        goodies[i]->pausePlay();
    }
    
    for (int i=0;i<vocals.size();i++){
        vocals [i]->pausePlay();
    }

}


//--------------------------------------------------------------
void Soundcontroller::setHeading(float _heading){
    heading=_heading;
    
}



void Soundcontroller::resetAllTickTargets(int _targetTicks, int _tickdeviation){

    goodiesTickTarget= tickcounter+ofRandom(_targetTicks-_tickdeviation,_targetTicks+_tickdeviation);
    goodiesTickInit = tickcounter;
    myGoodieSleepDelay=ofRandom(myGoodieSleepDelayInit-_tickdeviation*8,myGoodieSleepDelayInit+_tickdeviation*8);
    bIsSleepingGoodie=true;
    
    pianoTickTarget= tickcounter+ofRandom(_targetTicks-_tickdeviation,_targetTicks+_tickdeviation);
    pianoTickInit = tickcounter;
    myPianoSleepDelay=ofRandom(myPianoSleepDelayInit-_tickdeviation*8,myPianoSleepDelayInit+_tickdeviation*8);
    bIsSleepingPiano=true;
    
    bandoneonTickTarget= tickcounter+ofRandom(_targetTicks-_tickdeviation,_targetTicks+_tickdeviation);
    bandoneonTickInit = tickcounter;
    myBandonenonSleepDelay=ofRandom(myBandonenonSleepDelay-_tickdeviation*8,myBandonenonSleepDelay+_tickdeviation*8);
    bIsSleepingBandoneon=true;
    
    contrabassTickTarget= tickcounter+ofRandom(_targetTicks-_tickdeviation,_targetTicks+_tickdeviation);
    contrabassTickInit = tickcounter;
    myContrabassSleepDelay=ofRandom(myContrabassSleepDelayInit-_tickdeviation*8,myContrabassSleepDelayInit+_tickdeviation*8);
    bIsSleepingContrabass=true;
    
}



void Soundcontroller::drawArcStrip(float percent, ofVec2f center, float radius)
{
    float theta = ofMap(percent, 0, 1, 0, 360.0, true);
    
    float outerRadius=radius;
    float innerRadius=20;
    ofPushMatrix();
    //  ofTranslate(rect->getX(),rect->getY());
    
    ofBeginShape();
    
    {
        float x = sin(-ofDegToRad(0));
        float y = cos(-ofDegToRad(0));
        ofVertex(center.x+outerRadius*x,center.y+outerRadius*y);
    }
    
    for(int i = 0; i <= theta; i+=10)
    {
        float x = sin(-ofDegToRad(i));
        float y = cos(-ofDegToRad(i));
        
        ofVertex(center.x+outerRadius*x,center.y+outerRadius*y);
    }
    
    {
        float x = sin(-ofDegToRad(theta));
        float y = cos(-ofDegToRad(theta));
        ofVertex(center.x+outerRadius*x,center.y+outerRadius*y);
        ofVertex(center.x+innerRadius*x,center.y+innerRadius*y);
    }
    
    for(int i = theta; i >= 0; i-=10)
    {
        float x = sin(-ofDegToRad(i));
        float y = cos(-ofDegToRad(i));
        
        ofVertex(center.x+innerRadius*x,center.y+innerRadius*y);
    }
    
    {
        float x = sin(-ofDegToRad(0));
        float y = cos(-ofDegToRad(0));
        ofVertex(center.x+innerRadius*x,center.y+innerRadius*y);
    }
    
    ofEndShape();
    ofPopMatrix();
}



vector<ofVec2f> Soundcontroller::resample(int n,vector<ofVec2f> _line ) {
    vector<ofVec2f> points;
    points=_line;
    double len = 0;
    for(int i = 1; i < points.size(); ++i) {
        len += (points[i-1] - points[i]).length();
    }

    double I = len/(n - 1);
    double D = 0;
    
    vector<ofVec2f>  resampled_points;
    
    for(int i = 1; i < points.size(); ++i) {
        ofVec2f curr = points[i];
        ofVec2f prev = points[i-1];
        ofVec2f dir = prev - curr;
        double d = dir.length();
        if( (D + d) >= I) {
            double qx = prev.x + ((I-D)/d) * (curr.x - prev.x);
            double qy = prev.y + ((I-D)/d) * (curr.y - prev.y);
            ofVec2f resampled(qx, qy);
            resampled_points.push_back(resampled);
            points.insert(points.begin() + i, resampled);
            D = 0.0;
        }
        else {
            D += d;
        }
    }
    // we had to do some freaky resizing because of rounding issues.
    while(resampled_points.size() <= (n - 1)) {
        resampled_points.push_back(points.back());
    }
    if(resampled_points.size() > n) {
        resampled_points.erase(resampled_points.begin(), resampled_points.begin()+n);
    }
    
    return resampled_points;
}




//--------------------------------------------------------------
void Soundcontroller::touchDown(ofTouchEventArgs & touch){
  //  coreMotion.resetAttitude();



    if(touch.id==1){
     //   startBaseCycle();
      //  bLoopBaseCycle=true;
        
        
        gesture->reset();
        gesture2->reset();

        for(int i = 0; i < line.size(); ++i) {
            gesture->addPoint(line[i].x, line[i].y);
        }
        
        
        if(gesture->points.size() <= 10) {
            message = "Please add a line first";
            
        }
        else {
            dollar.addGesture(gesture);
            message = "Added gesture to recognizer";
            line.clear();
           // createNewGesture();
        }
        line.clear();
        dollar.save(ofxiPhoneGetDocumentsDirectory()+"gestures.txt");
        
        for(int i = 0; i < line2.size(); ++i) {
            gesture2->addPoint(line2[i].x, line2[i].y);
        }
        
        
        if(gesture2->points.size() <= 10) {
            message = "Please add a line first";
            
        }
        else {
            dollar2.addGesture(gesture2);
            message = "Added gesture to recognizer";
            line2.clear();
            createNewGesture();
        }
        line.clear();
        dollar.save(ofxiPhoneGetDocumentsDirectory()+"gestures2.txt");

        
    }
    
    if(touch.id==2){
       // stopBaseCycle();
        
    }
    
    
}

//--------------------------------------------------------------
void Soundcontroller::touchMoved(ofTouchEventArgs & touch){

    
    
}

//--------------------------------------------------------------
void Soundcontroller::touchUp(ofTouchEventArgs & touch){
    isTouched=false;
    gesture->reset();
    gesture2->reset();
    
    for(int i = 0; i < recline.size(); ++i) {
 //       gesture->addPoint(recline[i].x, recline[i].y);
    }
    
    
    if(gesture->points.size() <= 10) {
        message = "Please add a line first";
        
    }
    else {
        dollar.addGesture(gesture);
        message = "Added gesture to recognizer";
        line.clear();
        // createNewGesture();
    }
    recline.clear();
    dollar.save(ofxiPhoneGetDocumentsDirectory()+"gestures.txt");
    
    
    
    
    
    for(int i = 0; i < recline2.size(); ++i) {
       // gesture2->addPoint(recline2[i].x, recline2[i].y);
    }
    
    
    if(gesture2->points.size() <= 10) {
        message = "Please add a line first";
        
    }
    else {
        dollar2.addGesture(gesture2);
        message = "Added gesture to recognizer";
        recline2.clear();
        createNewGesture();
    }
    recline2.clear();
    dollar.save(ofxiPhoneGetDocumentsDirectory()+"gestures2.txt");

    
    
    
}

//--------------------------------------------------------------
void Soundcontroller::touchDoubleTap(ofTouchEventArgs & touch){
    
    //isTouched=true;
    recframebefore2=ofGetFrameNum();
    
    rollCounter=0;
    
    rollbuffer.clear();
    
    for(int i=0;i<50;i++){
        rollbuffer.push_back(0);
    }

    zbuffer.clear();
    for(int i=0;i<50;i++){
        zbuffer.push_back(0);
    }
    
   coreMotion.resetAttitude();
    initQuat = coreMotion.getQuaternion();

    maxAccelerationlength=0;
    maxSmooth_roll=0;
    
    myActualBaseBeat->removeFromQueue();
    //soundObjects[1]->removeFromQueue();
    //soundObjects[2]->removeFromQueue();

    
    heading_ref = ofLerpDegrees(heading, -coreLocation->getTrueHeading(), 0.7);
    
    
    float roll = atan2(2*initQuat.x()*initQuat.y() + 2*initQuat.w()*initQuat.z(), initQuat.w()*initQuat.w() + initQuat.x()*initQuat.x() - initQuat.y()*initQuat.y() - initQuat.z()*initQuat.z());
    
    quaternionRoll_ref=roll;
    
    
 //   soundObjects[1].removeFromQueue();

 //   baseBeats[0].resetPlay();


    
}

//--------------------------------------------------------------
void Soundcontroller::touchCancelled(ofTouchEventArgs & touch){
    
}


void Soundcontroller::mouseMoved(ofMouseEventArgs & args){}
void Soundcontroller::mouseDragged(ofMouseEventArgs & args){}
void Soundcontroller::mousePressed(ofMouseEventArgs & args){}
void Soundcontroller::mouseReleased(ofMouseEventArgs & args){}


vector<float> Soundcontroller::lowPass(vector<float> input, vector<float> output){
    
    //if (output == NULL ) return input;
    
    for ( int i=0; i<input.size(); i++ ) {
        output[i] = (input[i] * alpha) + (output[i] * (1.0 - alpha));
    }
    return output;
};


void Soundcontroller::setVariationOffset(int _offSet){
    variationOffset=_offSet;
}




int Soundcontroller::getVariationOffset(){
    return variationOffset;
}

void Soundcontroller::switchVariationBaseBeat(int _offset){
    setVariationOffset(_offset);
    myActualBaseBeat->switchPlay();
}


void Soundcontroller::setChordOffset(int _offSet){
    chordOffset=_offSet;
}


int Soundcontroller::getChordOffset(){
    return chordOffset;
}


int Soundcontroller::getTickcounter(){
    return tickcounter;
}

void Soundcontroller::setActualBaseBeat(SoundObject * so){
    if(so==myActualBaseBeat)return;
    myActualBaseBeat=so;
    baseCycleDuration=myActualBaseBeat->getMySoundLength()-20;
    cout<<"base---"<<baseCycleDuration<<endl;
}


SoundObject* Soundcontroller::getActualBaseBeat(){
    return myActualBaseBeat;
}




void Soundcontroller::setNextBaseBeat(SoundObject * so){
    if(so==myActualBaseBeat)return;
    if(so==myNextBaseBeat)return;
    myNextBaseBeat=so;
    nextBaseCycleDuration=myNextBaseBeat->getMySoundLength()-20;
    cout<<"base---"<<baseCycleDuration<<endl;
}


SoundObject* Soundcontroller::getNextBaseBeat(){
    return myNextBaseBeat;
}






void Soundcontroller::setIsBreak(bool _isbreak){
        bIsBreak=_isbreak;
}


bool Soundcontroller::getIsBreak(){
    return bIsBreak;
}


void Soundcontroller::setIsParallel(bool _isParallel){
 //   if(parallelTickCounter+parallelTickCounterdiff<tickcounter){
    bIsParallel=_isParallel;
    /*if(bIsParallel){
        int index=getNextBaseBeat()->getIndex();
        if(index<parallels.size()){
        setNextBaseBeat(parallels[index]);
        }
    }else{
        int index=getNextBaseBeat()->getIndex();
        setNextBaseBeat(baseBeats[index]);
    }*/
   // parallelTickCounter=tickcounter;
   // }
}

bool Soundcontroller::getIsParallel(){
    return bIsParallel;
}



void Soundcontroller::setBaseBeatIndex(int _index){
    myActualBaseBeatIndex= _index;
}


int Soundcontroller::getBaseBeatIndex(){
    return myActualBaseBeatIndex;
}



void Soundcontroller::addRandomVocalsInstantPlay(bool _sleep,int _sleeptime){
    if((bIsSleepingVocal && _sleep )|| isShortSleep)return;
        int index =floor(ofRandom(vocals.size()));
    vocals[index]->setSleepTime(_sleeptime);
       vocals[index]->instantPlay();
   
    vocalSleepStartTime=ofGetElapsedTimeMillis();
    bIsSleepingVocal=true;
    
    isShortSleep=true;
    shortSleepStartTime=ofGetElapsedTimeMillis();


}


void Soundcontroller::addRandomSoundObjectInstantPlay(bool _sleep, int _sleeptime){
    if((bIsSleepingSoundObjects && _sleep)|| isShortSleep)return;
    
    int index =floor(ofRandom(soundObjects.size()));
    
    /*  while(soundObjects[index]->getIsSleeping()){
     cout<<"---------------------Check another"<<endl;
     //index =floor(ofRandom(soundObjects.size()));
     }*/
    
    soundObjects[index]->setSleepTime(_sleeptime);
    soundObjects[index]->instantPlay();
    
    isShortSleep=true;
    shortSleepStartTime=ofGetElapsedTimeMillis();
    
    soundObjectSleepStartTime=ofGetElapsedTimeMillis();
    bIsSleepingSoundObjects=true;
}



void Soundcontroller::addRandomGoodieInstantPlay(bool _sleep,int _sleeptime){
    
    if((bIsSleepingGoodie && _sleep) || isShortSleep)return;
    int index =floor(ofRandom(goodies.size()));
    
   // while(goodies[index]->getIsSleeping()){
    //     index =floor(ofRandom(goodies.size()));
   // }
   
    goodies[index]->setSleepTime(_sleeptime);
   goodies[index]->instantPlay();
    
    
    goodieSleepStartTime=ofGetElapsedTimeMillis();
    bIsSleepingGoodie=true;
   
    isShortSleep=true;
    shortSleepStartTime=ofGetElapsedTimeMillis();

}


void Soundcontroller::addRandomGoodieToQueue(bool _sleep,int _sleeptime){
    if((bIsSleepingGoodie && _sleep)||isShortSleep)return;
    int index =floor(ofRandom(goodies.size()));
    if( goodies[index]->getIsSleeping()){
         index =floor(ofRandom(goodies.size()));
    }
    goodies[index]->setSleepTime(_sleeptime);
    goodies[index]->addToQueue();
    
    bIsSleepingGoodie=true;
    goodieSleepStartTime=ofGetElapsedTimeMillis();
   
    isShortSleep=true;
    shortSleepStartTime=ofGetElapsedTimeMillis();

}

void Soundcontroller::startRandomCrescendoTicker(int _delay, int _target){
    myGoodieSleepDelay=_delay;
    goodiesTickTarget=tickcounter+_target;
    goodiesTickInit=tickcounter;
    bIsSleepingGoodie=false;
}


void Soundcontroller::addPianoGoodieToQueue(bool _specialsleep, int _sleeptime, bool _sleep){
    if((bIsSleepingGoodie && _sleep)||isShortSleep || (bIsSleepingPiano && _specialsleep))return;
    
    int index =floor(ofRandom(0,5.9));
    if(goodies[index]->getIsSleeping())index =floor(ofRandom(0,5.9));
    goodies[index]->setSleepTime(_sleeptime);
   goodies[index]->addToQueue();
    
    bIsSleepingPiano=true;
    pianoSleepStartTime=ofGetElapsedTimeMillis();
    
    bIsSleepingGoodie=true;
    goodieSleepStartTime=ofGetElapsedTimeMillis();
    
    isShortSleep=true;
    shortSleepStartTime=ofGetElapsedTimeMillis();
}

void Soundcontroller::startPianoCrescendoTicker(int _delay, int _target){
    myPianoSleepDelay=_delay;
    pianoTickTarget=tickcounter+_target;
    pianoTickInit=tickcounter;
    bIsSleepingPiano=false;
}







void Soundcontroller::addPianoGoodieToPlayAt(bool _specialsleep, int _sleeptime, bool _sleep){
    if((bIsSleepingGoodie && _sleep)||isShortSleep || (bIsSleepingPiano && _specialsleep))return;
    if(!bIsBreak){
    int index =floor(ofRandom(0,5.9));
        if(goodies[index]->getIsSleeping())index =floor(ofRandom(0,5.9));

        goodies[index]->setSleepTime(_sleeptime);
        goodies[index]->startPlayAt(getBaseCyclePos());
        bIsSleepingGoodie=true;
        goodieSleepStartTime=ofGetElapsedTimeMillis();
        
        isShortSleep=true;
        shortSleepStartTime=ofGetElapsedTimeMillis();
        
        bIsSleepingPiano=true;
        pianoSleepStartTime=ofGetElapsedTimeMillis();
    }
    
  
}





void Soundcontroller::addBandoneonGoodieToQueue(bool _specialsleep, int _sleeptime, bool _sleep){
    
    if((bIsSleepingGoodie && _sleep)||isShortSleep || (bIsSleepingBandoneon && _specialsleep))return;
    int index =floor(ofRandom(11,14.9));
    if(goodies[index]->getIsSleeping())index =floor(ofRandom(11,14.9));

    
    
    goodies[index]->setSleepTime(_sleeptime);
   goodies[index]->addToQueue();
    bIsSleepingGoodie=true;
    goodieSleepStartTime=ofGetElapsedTimeMillis();
    
    isShortSleep=true;
    shortSleepStartTime=ofGetElapsedTimeMillis();
    
    
    bIsSleepingBandoneon=true;
    bandoneonSleepStartTime=ofGetElapsedTimeMillis();
}

void Soundcontroller::addBandoneonGoodieToPlayAt(bool _specialsleep, int _sleeptime, bool _sleep){
    if((bIsSleepingGoodie && _sleep)||isShortSleep || (bIsSleepingBandoneon && _specialsleep))return;
    if(!bIsBreak){
        int index =floor(ofRandom(11,14.9));
        if(goodies[index]->getIsSleeping())index =floor(ofRandom(11,14.9));

        goodies[index]->setSleepTime(_sleeptime);

        goodies[index]->startPlayAt(getBaseCyclePos());
        bIsSleepingGoodie=true;
        goodieSleepStartTime=ofGetElapsedTimeMillis();
        
        isShortSleep=true;
        shortSleepStartTime=ofGetElapsedTimeMillis();
        
        bIsSleepingBandoneon=true;
        bandoneonSleepStartTime=ofGetElapsedTimeMillis();
    }
}


void Soundcontroller::startBandoneonCrescendoTicker(int _delay, int _target){
    myBandonenonSleepDelay=_delay;
    bandoneonTickTarget=tickcounter+_target;
    bandoneonTickInit=tickcounter;
    bIsSleepingBandoneon=false;
}




void Soundcontroller::addContrabassGoodieToQueue(bool _specialsleep, int _sleeptime, bool _sleep){
    if((bIsSleepingGoodie && _sleep)||isShortSleep || (bIsSleepingContrabass && _specialsleep)) return;
    int index =floor(ofRandom(6.1,7.9));
    if(goodies[index]->getIsSleeping())index =floor(ofRandom(6.1,7.9));

    goodies[index]->setSleepTime(_sleeptime);
    goodies[index]->addToQueue();
    bIsSleepingGoodie=true;
    goodieSleepStartTime=ofGetElapsedTimeMillis();
    
    isShortSleep=true;
    shortSleepStartTime=ofGetElapsedTimeMillis();
    
    bIsSleepingContrabass=true;
    contrabassSleepStartTime=ofGetElapsedTimeMillis();
}


void Soundcontroller::addContrabassGoodieToPlayAt(bool _specialsleep, int _sleeptime, bool _sleep){
    if((bIsSleepingGoodie && _sleep)||isShortSleep || (bIsSleepingContrabass && _specialsleep))return;
    if(!bIsBreak){
        int index =floor(ofRandom(6.1,7.9));
        goodies[index]->setSleepTime(_sleeptime);

        goodies[index]->startPlayAt(getBaseCyclePos());
        bIsSleepingGoodie=true;
        goodieSleepStartTime=ofGetElapsedTimeMillis();
        
        isShortSleep=true;
        shortSleepStartTime=ofGetElapsedTimeMillis();
        
        bIsSleepingContrabass=true;
        contrabassSleepStartTime=ofGetElapsedTimeMillis();
    }
}


void Soundcontroller::startContrabassCrescendoTicker(int _delay, int _target){
    myContrabassSleepDelay=_delay;
    contrabassTickTarget=tickcounter+_target;
    contrabassTickInit=tickcounter;
    bIsSleepingContrabass=false;
}


void Soundcontroller::addPercussionGoodieToQueue(bool _sleep,int _sleeptime){
    if((bIsSleepingGoodie && _sleep)||isShortSleep)return;

    int index =floor(ofRandom(9.1,10.9));
   goodies[index]->addToQueue();

    bIsSleepingGoodie=true;
    goodieSleepStartTime=ofGetElapsedTimeMillis();
    
    isShortSleep=true;
    shortSleepStartTime=ofGetElapsedTimeMillis();
}


void Soundcontroller::addRomanticDoublerToQueue(bool _sleep,int _sleeptime){
    if((bIsSleepingGoodie && _sleep)||isShortSleep)return;
    doublers[0]->addToQueue();
    bIsSleepingDouble=true;
    doublerSleepStartTime=ofGetElapsedTimeMillis();
    
    isShortSleep=true;
    shortSleepStartTime=ofGetElapsedTimeMillis();
}

void Soundcontroller::addDoublerToQueue(int index,bool _sleep,int _sleeptime){
    if((bIsSleepingGoodie && _sleep)||isShortSleep )return;
    doublers[index]->addToQueue();
    bIsSleepingDouble=true;
    doublerSleepStartTime=ofGetElapsedTimeMillis();
    isShortSleep=true;
    shortSleepStartTime=ofGetElapsedTimeMillis();
}




void Soundcontroller::resetHeadingBuffer(){
    headingbuffer.clear();
    for(int i=0;i<100;i++){
        headingbuffer.push_back(heading_angle);
    }
}



void Soundcontroller::resetXCounter(){
    xCounter=0;
}

void Soundcontroller::resetYCounter(){
    yCounter=0;
}


void Soundcontroller::resetZCounter(){
    z2Counter=0;
}

vector<ofColor> Soundcontroller::generateColorSwatchesFromColor(float _num, float _huerange, float _saturationrange, float _brightnessrange, ofColor _col){
    
    vector<ofColor> c;
    int num=_num;
    float hue=_col.getHue();
    int sat=_col.getSaturation();
    int brightness=_col.getBrightness();
    
    float deltahue=hue-_huerange;
    
    // cout<<hue<<" "<<deltahue<<endl;
    
    float saturationrange=_saturationrange;
    float brightnessrange=_brightnessrange;
    
    float deltabrightness=100;
    float deltasat;
    
    float hueratio=_huerange/_num;
    ofColor col;
    
    for (int i=1;i<=num;i++){
        ofColor col;
        deltahue+=hueratio*i;
        
        deltabrightness=ofRandom(brightness-brightnessrange,brightness+brightnessrange);
        deltabrightness=ofClamp(deltabrightness, 0, 255);
        
        deltasat=ofRandom(sat-saturationrange,sat+saturationrange);
        deltasat=ofClamp(deltasat, 0, 255);
        
        if(deltahue<0)deltahue=255+deltahue;
        if(deltahue>255)deltahue=255-deltahue;
        col.setHsb(deltahue, deltasat, deltabrightness);
        
        c.push_back(col);
    }
    return c;
}




